Imports System.Text.RegularExpressions
Imports System.Configuration
Imports ntb_FuncLib.LogFile

Module ConvertTools

    'Types of events
    Public Enum NameFormat
        NBSP
        UCASE
        BOTH
    End Enum

    'Fixes names: XXXX yyyy zzz -> Yyyy Zzzz Xxxx
    'Rotates and captial case's

    Function RotateFISName(ByVal name As String) As String
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        Dim nreg As New Regex("(([A-Z����������������]+[^A-Za-z����������������]+)+)+(([A-Z�������]{1}[a-z�����������������\s+\.\-]+)+)", _
                              RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim m As Match
        Dim strReturnName As String = ""

        m = nreg.Match(name)

        If m.Success() Then


            name = m.Groups(3).Value.Trim().ToString() & " " & m.Groups(1).Value.Trim().ToString()
        Else
            Dim strSplittedName As String() = name.Split(" ")
            If strSplittedName.Length = 2 Then
                strReturnName = strSplittedName(1).ToString() & " " & strSplittedName(0).ToString()
            ElseIf strSplittedName.Length >= 3 Then
                Dim i As Integer = 0
                For i = 1 To strSplittedName.Length - 1
                    If Debug Then
                        WriteLog(logFolder, "strSplittedName(" & i & "): " & strSplittedName(i).ToString())
                    End If

                    strReturnName &= strSplittedName(i).ToString() & " "
                Next
                strReturnName &= strSplittedName(0).ToString()

            End If
        End If
        strReturnName = StrConv(name, vbProperCase)
        Return strReturnName
    End Function
    Function RotateAndFixName(ByVal name As String, Optional ByVal format As NameFormat = NameFormat.BOTH, Optional ByVal lastNameFirst As Boolean = False) As String
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))

        If Debug Then
            WriteLog(logFolder, "ConvertTools.RotateAndFixName")
        End If

        Dim nreg As New Regex("^([A-Z����� \-]+)\b ([\w a-z������ \-]+)")
        Dim m As Match


        'Rotate first/last
        If Debug = True Then
            WriteLog(logFolder, "Rotate first/last")
        End If

        If format = NameFormat.BOTH Or format = NameFormat.UCASE Then
            m = nreg.Match(name)
            If m.Success() Then
                name = m.Groups(2).Value & " " & m.Groups(1).Value.ToLower
            End If
        End If

        If format = NameFormat.BOTH Or format = NameFormat.NBSP Then
            If name.IndexOf("&nbsp;") > -1 Then
                Dim splitter() As Char = "&;"
                name = name.Split(splitter, 3)(2) & " " & name.Split(splitter, 2)(0)
                name = name.Replace("&nbsp;", " ")
            End If
        End If

        name = name.ToLower

        'Uppercase -> Capital Case
        ' @todo: Fix the Rotation - there is something wrong right here ->
        If Debug = True Then WriteLog(logFolder, "Uppercase -> Capital Case")

        name = StrConv(name, vbProperCase)
        'nreg = New Regex("(\b|[ \-])(\w)")
        'm = nreg.Match(name)
        'While m.Success()
        '    name = name.Insert(m.Groups(2).Index, m.Groups(2).Value.ToUpper)
        '    name = name.Remove(m.Groups(2).Index + 1, 1)
        '    m = m.NextMatch()
        'End While
        'If Debug = True Then WriteLog(logFolder, "Ferdig med Uppercase -> Capital Case")

        'Dim delimStr As String = " "
        'Dim delimiter As Char() = delimStr.ToCharArray()
        'Dim split As String() = Nothing

        Dim s As String = Nothing

        'Split = name.Split(delimiter)
        'name = ""

        'For Each s In Split()
        's = s.Insert(0, s.Substring(0, 1).ToUpper())
        's = s.Remove(1, 1)
        'name = name & s & " "
        'Next

        'name = name.Trim(delimiter)
        If Debug = True Then WriteLog(logFolder, "Lastname first")
        If lastNameFirst = True Then
            ' Weijiang AN
            ' Det vi skal gj�re her er � flytte AN f�rst. 
            ' Med andre ord er det bare � flytte det bakerste navnet f�rst

            ' Ola Vigen Hattestad
            Dim split As String() = Nothing

            split = name.Split(" ")
            ' N� har vi
            ' Hattestad
            ' Ola
            ' Vigen
            Dim strName As String = ""

            name = split(UBound(split)) & " " & split(LBound(split))

            'If lastNameFirst Then
            'name = name.Remove(name.LastIndexOf(s) - 1, s.Length + 1)
            'name = s & " " & name
            'End If
        End If
        'WriteLog(logFolder, "Ferdig lastname first")
        'WriteLog(logFolder, name)

        Return name
    End Function

    Function FixName(ByVal strName As String) As String

        Dim nreg As Regex = New Regex("(\b|[ \-])(\w)")
        Dim m As Match = nreg.Match(strName)
        While m.Success()
            strName = strName.Insert(m.Groups(2).Index, m.Groups(2).Value.ToUpper)
            strName = strName.Remove(m.Groups(2).Index + 1, 1)
            m = m.NextMatch()
        End While

        Return strName

    End Function
    'Converts a time into a timespan
    Function GetTimeSpan(ByVal time As String, Optional ByVal milli_mul As Integer = 100) As TimeSpan
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))

        Try

            Dim ret As New TimeSpan(0)

            ' WriteLog(logFolder, "GetTimeSpan: This is input: " & time.ToString)
            ' We change if we have , in the time. it shall not be
            time = Replace(time.ToString, ",", ".")

            'WriteLog(logFolder, "GetTimeSpan: This is input after replace: " & time.ToString)

            Dim diffRx As Regex = New Regex("((\d+).)?((\d+).)?(\d+).(\d+)")
            Dim diffM As Match = diffRx.Match(time)
            If diffM.Success Then
                Dim h As Integer = 0
                Dim m As Integer = 0
                Dim s As Integer = 0
                Dim t As Integer = 0

                If diffM.Groups(2).Value <> "" Then h = diffM.Groups(2).Value
                If diffM.Groups(4).Value <> "" Then
                    m = diffM.Groups(4).Value
                Else
                    m = h
                    h = 0
                End If

                If diffM.Groups(5).Value <> "" Then s = diffM.Groups(5).Value
                If diffM.Groups(6).Value <> "" Then t = diffM.Groups(6).Value

                ' ret = New TimeSpan(0, h, m, s, t * 10)
                ret = New TimeSpan(0, h, m, s, t * milli_mul)
                ' ret = New TimeSpan(0, h, m, s, t)
            End If
            'WriteLog(logFolder, "This is what we return from GetTimeSpan: " & ret.ToString)
            Return ret

        Catch exNull As NoNullAllowedException
            WriteLog(logFolder, "An NoNullAllowedException has occured: " + exNull.Message.ToString())

            WriteLog(logFolder, "An NoNullAllowedException has occured: " + exNull.StackTrace.ToString())

            Return Nothing
        Catch exNull As NullReferenceException
            WriteLog(logFolder, "An NullAllowedException has occured: " + exNull.Message.ToString())

            WriteLog(logFolder, "An NullAllowedException has occured: " + exNull.StackTrace.ToString())

            Return Nothing
        Catch e As Exception
            WriteLog(logFolder, "An Exception has occured: " + e.Message.ToString())

            WriteLog(logFolder, "An Exception has occured: " + e.StackTrace.ToString())

            Return Nothing
        End Try
    End Function

    'Formats times according to NTB specs
    Function FormatTime(ByVal span As TimeSpan, Optional ByVal milli_div As Integer = 100, Optional ByVal milli_digits As Integer = 1) As String
        ' Added 100 to the integer to get the correct number out when parsing Cross Country mass start (Langrenn Felles)
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim ret As String
        'WriteLog(logFolder, "FormatTime: This is input: " & span.ToString)

        If span.Hours > 0 Then
            ret = String.Format("{0}.{1:D2}.{2:D2},{3:D" & milli_digits & "}", span.Hours, span.Minutes, span.Seconds, span.Milliseconds \ milli_div)
            'ElseIf time.Minutes > 0 Then
            '    strTime = String.Format("{0}.{1:D2},{2}", time.Minutes, time.Seconds, time.Milliseconds / 100)
        Else
            'strTime = String.Format("{0},{1}", time.Seconds, time.Milliseconds / 100)
            ret = String.Format("{0}.{1:D2},{2:D" & milli_digits & "}", span.Minutes, span.Seconds, span.Milliseconds \ milli_div)
        End If
        'WriteLog(logFolder, "This is what we return from FormatTime: " & ret.ToString)
        Return ret
    End Function

    'Formats times Alpine according to NTB specs
    Function FormatTimeAl(ByVal span As TimeSpan, Optional ByVal milli_div As Integer = 1) As String

        Dim ret As String

        If span.Hours > 0 Then
            ret = String.Format("{0}.{1:D2}.{2:D2},{3}", span.Hours, span.Minutes, span.Seconds, span.Milliseconds / 100)
            'ElseIf time.Minutes > 0 Then
            '    strTime = String.Format("{0}.{1:D2},{2}", time.Minutes, time.Seconds, time.Milliseconds / 100)
        Else
            'strTime = String.Format("{0},{1}", time.Seconds, time.Milliseconds / 100)
            ret = String.Format("{0}.{1:D2},{2:D2}", span.Minutes, span.Seconds, span.Milliseconds \ milli_div)
        End If

        Return ret
    End Function

    'Formats times Alpine according to NTB specs
    Function FormatTimeSkate(ByVal span As TimeSpan, Optional ByVal milli_div As Integer = 1) As String

        Dim ret As String

        If span.Minutes > 0 Then
            ret = String.Format("{0}.{1:D2},{2:D2}", span.Minutes, span.Seconds, span.Milliseconds \ milli_div)
        Else
            ret = String.Format("{0:D2},{1:D2}", span.Seconds, span.Milliseconds \ milli_div)
        End If

        Return ret
    End Function

    Function GetPenaltyAndExtraShotsBiathlon(ByVal strShotsAndPenalty As String) As String
        strShotsAndPenalty = strShotsAndPenalty.Replace("  ", " ")
        Dim arrShotsAndPenalty As String() = strShotsAndPenalty.Split(" ")
        Dim arrFirstShooting As String() = arrShotsAndPenalty(0).Split("+")
        Dim arrSecondShooting As String() = arrShotsAndPenalty(1).Split("+")

        Dim intFirst As Integer = Convert.ToInt32(arrFirstShooting(0).ToString())
        Dim intSecond As Integer = Convert.ToInt32(arrFirstShooting(1).ToString())
        Dim intThird As Integer = Convert.ToInt32(arrSecondShooting(0).ToString())
        Dim intFourth As Integer = Convert.ToInt32(arrSecondShooting(1).ToString())

        Dim intPenaltylaps As Integer = intFirst + intThird
        Dim intExtraShots As Integer = intSecond + intFourth
        GetPenaltyAndExtraShotsBiathlon = intPenaltylaps.ToString() & "/" & intExtraShots.ToString()
    End Function

End Module
