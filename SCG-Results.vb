Imports System.IO
Imports System.xml
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports ntb_FuncLib.LogFile

Public Class SCG_Results

    Public ErrorFolder As String

    Protected Class athlete
        'General
        Public rank As String
        Public bib As String
        Public name As String
        Public nat_code As String
        Public nation As String
        'Speedskating
        Public time As String
        Public diff As String

        'Construktor 
        'Speedskating
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String)
            Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
            Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
            Dim boolFixname As Boolean = True
            If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.sub New")
            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in

            If Debug = True Then
                WriteLog(logFolder, "Vi har: Rank" & rank)
                WriteLog(logFolder, "Vi har bib: " & bib)
                WriteLog(logFolder, "Vi har name: " & name)
                WriteLog(logFolder, "Vi har nat_code: " & nat_code)
                WriteLog(logFolder, "Vi har diff: " & diff)
                WriteLog(logFolder, "Vi har: " & rank & " " & bib & " " & name & " " & nat_code & " " & diff)
            End If

            nation = MainConvertModule.NationMap(nat_code)
            If nation = "" Then nation = natcode_in
            If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation)
            
            Dim lastNameFirst As Boolean
            lastNameFirst = False
            If nation Like "Kina" Or nation Like "S�r-Korea" Or nation Like "Nord-Korea" Then
                If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation & " og setter lastnamefirst til true")
                lastNameFirst = True
            ElseIf nation Like "Nederland" Then
                ' Do not change van to Van or De to de or Der to der and so on.
                ' I do see that lastname is all uppercase, so it is only needed to find names that are all uppercase...
                boolFixname = False
            Else
                If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation & " og setter lastnamefirst til false")
                lastNameFirst = False
            End If

            If boolFixname = True Then
                name = FixName(name)
            End If

            If Debug = True Then
                WriteLog(logFolder, "Vi snur navn")
                WriteLog(logFolder, "Vi har name: " & name)
            End If




            If Debug = True Then WriteLog(logFolder, "Character map substitutions")
            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Debug = True Then WriteLog(logFolder, "subst")
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
                If Debug = True Then WriteLog(logFolder, "Ferdig med for loop")
            End If
        End Sub

        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal time_in As String, ByVal diff_in As String)
            Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
            Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
            Dim boolFixname As Boolean = True
            If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.sub New")
            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in
            diff = diff_in
            If Debug = True Then
                WriteLog(logFolder, "Vi har: Rank" & rank)
                WriteLog(logFolder, "Vi har bib: " & bib)
                WriteLog(logFolder, "Vi har name: " & name)
                WriteLog(logFolder, "Vi har nat_code: " & nat_code)
                WriteLog(logFolder, "Vi har diff: " & diff)
                WriteLog(logFolder, "Vi har: " & rank & " " & bib & " " & name & " " & nat_code & " " & diff)
            End If

            nation = MainConvertModule.NationMap(nat_code)
            If nation = "" Then nation = natcode_in
            If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation)
            time = time_in

            Dim lastNameFirst As Boolean
            lastNameFirst = False
            If nation Like "Kina" Or nation Like "S�r-Korea" Or nation Like "Nord-Korea" Then
                If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation & " og setter lastnamefirst til true")
                lastNameFirst = True
            ElseIf nation Like "Nederland" Then
                ' Do not change van to Van or De to de or Der to der and so on.
                ' I do see that lastname is all uppercase, so it is only needed to find names that are all uppercase...
                boolFixname = False
            Else
                If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation & " og setter lastnamefirst til false")
                lastNameFirst = False
            End If

            If boolFixname = True Then
                name = ConvertTools.FixName(name)
            End If

            If Debug = True Then
                WriteLog(logFolder, "Vi snur navn")
                WriteLog(logFolder, "Vi har name: " & name)
            End If




            If Debug = True Then WriteLog(logFolder, "Character map substitutions")
            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Debug = True Then WriteLog(logFolder, "subst")
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
                If Debug = True Then WriteLog(logFolder, "Ferdig med for loop")
            End If

        End Sub

    End Class

    Protected athlete_list As ArrayList
    Protected Shared character_map As New Hashtable

    'Object constructor creates object and loads mapping lists
    Public Sub New()

        athlete_list = New ArrayList

        'Load character map
        Dim charmapfile As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("CharacterMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(charmapfile)

            Dim nodes As XmlNodeList
            Dim nd1, nd2 As XmlNode
            nodes = map.SelectNodes("/character-maps/nation")

            'Fill hash
            For Each nd1 In nodes
                If nd1.ChildNodes().Count > 0 Then

                    Dim c As Integer = 0
                    Dim charset((nd1.ChildNodes().Count * 2) - 1) As String
                    For Each nd2 In nd1.ChildNodes()
                        charset(c) = nd2.Attributes("value").Value
                        charset(c + 1) = nd2.Attributes("replace").Value
                        c += 2
                    Next

                    character_map(nd1.Attributes("code").Value) = charset
                End If
            Next

            WriteLog(New String(MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))), "SCG - Character converison map loaded (" & charmapfile & "): " & CStr(character_map.Count) & " conversion sets.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(New String(MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))), "Error loading charachter conversion map (" & charmapfile & ") for IBU", ex)

            Throw New Exception(ex.Message)
        End Try

    End Sub

    Protected Function ParseDataSCGSprint(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.ParseDataSCG")

        Dim e As Encoding
        e = Encoding.GetEncoding(job.FileEncoding.ToUpper())
        If job.FileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If


        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        ' Remove span
        ' 
        If Debug = True Then WriteLog(logFolder, "Remove tags")
        Dim span As Regex = New Regex("<span\b[^>]*>(\d)+(.*?)</span>", RegexOptions.Compiled) 'Drop all tags
        content = span.Replace(content, " ")
        If Debug = True Then
            WriteLog(logFolder, content)
        End If
        'Remove tags
        If Debug = True Then
            WriteLog(logFolder, "Remove tags")
        End If
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, " ")
        If Debug = True Then
            WriteLog(logFolder, content)
        End If
        'Split off header
        If Debug = True Then WriteLog(logFolder, "Split off header")
        Dim topsplit As String = "Rankings"
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results
        If Debug = True Then WriteLog(logFolder, "Find results with RegExp")
        ' Dim res As Regex = New Regex("(\d+)\s+(\d+)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)(\(\d\))?\s+([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' Dim res As Regex = New Regex("(\d+)\s+(\d+)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)(\(\d\))?\s+([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        ' Dim res As Regex = New Regex("([0-9]+)\s+([0-9]+)\s+([a-z-����������]+\s+)\s+([a-z]{3})\s+([0-9]+:[0-9]+\.[0-9]+|[0-9]+\.[0-9]+|[0-9]+:[0-9]+\.[0-9]+)\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' Dim res As Regex = New Regex("([0-9]+)\s+([0-9]+)\s+(([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+)\s+(([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+([a-z-����������]+)\s+([a-z-����������]+)\s+)\s+([a-z]{3})\s+([0-9]+:[0-9]+\.[0-9]+|[0-9]+\.[0-9]+|[0-9]+:[0-9]+\.[0-9]+)\s+", _
        '                             RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        ' Finding Team Tempo


        ' Dim res As Regex = New Regex("([0-9]{1,3})\s+([0-9]{1,3})\s+(.*[^\s+0-9])\s+(.*[^\s+0-9])\s+([A-Z]{3})\s+([0-9]{1,2}:[0-9]{2}\.[0-9]{2}|[0-9]{2}\.[0-9]{2})\s+", _

        Dim res As Regex = New Regex(job.Regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim m As Match = res.Match(content)

        ' 
        ' the line below is moved out as of 25.11.2010 because it does not work with this sport anymore.
        ' (\d+)\s+(\d+)\s+([\w\- ]+?)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)\s+([\d:\.]+)\s+(\d+)\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' (\d+)\s+(\d+)\s+(\w+)\s+(\w+)\s+(\w{3})\s+([\d:\.]+)\s
        ' (\d+)\s+(\d+)\s+(\w+)\s+(\w+|[a-z]+-[a-z]+)\s+(\w{3})\s+(\d{2}\.)(\d{2})\s+
        ' \s(\w{3})\s+((\d{2}\.)(\d{2}))\s+
        ' \s(\w{3})\s+((\d{2}\.\d{2}))\s+
        Dim first As TimeSpan = Nothing
        Dim i As Integer
        While m.Success

            If Debug = True Then
                For i = 1 To m.Groups.Count - 1
                    WriteLog(logFolder, "output of " & i.ToString() & ": " & m.Groups(i).Value.ToString())
                Next
            End If

            Dim rank As String = ""
            Dim bib As String = ""
            Dim name As String = ""
            Dim nation As String = ""
            Dim strTime As String = ""
            Dim strDiff As String = ""
            Dim firstname As String = ""
            Dim lastname As String = ""

            rank = m.Groups(1).Value
            bib = m.Groups(2).Value
            ' Dim name As String = m.Groups(3).Value
            ' Dim nation As String = m.Groups(4).Value

            firstname = m.Groups(3).Value.Trim().Replace(vbCrLf, "")
            lastname = m.Groups(4).Value.Trim().Replace(vbCrLf, "")


            nation = m.Groups(5).Value.ToString() ' endret fra 4 til 18
            strTime = m.Groups(6).Value.ToString()

            If nation = "NED" Then
                ' We shall change the name based on some rules
                Dim lastnameNederland As Array = lastname.Split(" ")

                ' empty lastname string
                lastname = ""
                lastnameNederland(UBound(lastnameNederland)) = StrConv(lastnameNederland(UBound(lastnameNederland)), VbStrConv.ProperCase)

                Dim forCount As Integer = 0

                For forCount = 0 To UBound(lastnameNederland)
                    lastname &= lastnameNederland(forCount) & " "
                Next
                lastname = lastname.Trim()
                WriteLog(logFolder, "Lastname: " & lastname)

                name = StrConv(firstname, VbStrConv.ProperCase) & " " & lastname


            Else
                name = firstname & " " & lastname
                name = StrConv(name, VbStrConv.ProperCase)
            End If

            If Debug = True Then
                WriteLog(logFolder, "Data vi har: " & rank & " " & bib & " " & name & " " & nation)
            End If

            strTime = strTime.Replace(".", ",").ToString()
            strTime = strTime.Replace(":", ".").ToString()
            strDiff = ""

            Dim athlete As New athlete(rank, bib, name, nation)

            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        Return athlete_list.Count
    End Function

    'Takes a speedskating HTML list and fills the athlete list
    Protected Function ParseDataSCG(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.ParseDataSCG")

        Dim e As Encoding
        e = Encoding.GetEncoding(job.FileEncoding.ToUpper())
        If job.FileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If


        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        ' Remove span
        ' 
        If Debug = True Then WriteLog(logFolder, "Remove tags")
        Dim span As Regex = New Regex("<span\b[^>]*>(\d)+(.*?)</span>", RegexOptions.Compiled) 'Drop all tags
        content = span.Replace(content, " ")
        If Debug = True Then
            WriteLog(logFolder, content)
        End If
        'Remove tags
        If Debug = True Then
            WriteLog(logFolder, "Remove tags")
        End If
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, " ")
        If Debug = True Then
            WriteLog(logFolder, content)
        End If
        'Split off header
        If Debug = True Then WriteLog(logFolder, "Split off header")
        Dim topsplit As String = "Rankings"
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results
        If Debug = True Then WriteLog(logFolder, "Find results with RegExp")
        ' Dim res As Regex = New Regex("(\d+)\s+(\d+)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)(\(\d\))?\s+([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' Dim res As Regex = New Regex("(\d+)\s+(\d+)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)(\(\d\))?\s+([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        ' Dim res As Regex = New Regex("([0-9]+)\s+([0-9]+)\s+([a-z-����������]+\s+)\s+([a-z]{3})\s+([0-9]+:[0-9]+\.[0-9]+|[0-9]+\.[0-9]+|[0-9]+:[0-9]+\.[0-9]+)\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' Dim res As Regex = New Regex("([0-9]+)\s+([0-9]+)\s+(([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+)\s+(([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+([a-z-����������]+)\s+([a-z-����������]+)\s+)\s+([a-z]{3})\s+([0-9]+:[0-9]+\.[0-9]+|[0-9]+\.[0-9]+|[0-9]+:[0-9]+\.[0-9]+)\s+", _
        '                             RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        ' Finding Team Tempo


        ' Dim res As Regex = New Regex("([0-9]{1,3})\s+([0-9]{1,3})\s+(.*[^\s+0-9])\s+(.*[^\s+0-9])\s+([A-Z]{3})\s+([0-9]{1,2}:[0-9]{2}\.[0-9]{2}|[0-9]{2}\.[0-9]{2})\s+", _

        Dim res As Regex = New Regex(job.Regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim m As Match = res.Match(content)

        ' 
        ' the line below is moved out as of 25.11.2010 because it does not work with this sport anymore.
        ' (\d+)\s+(\d+)\s+([\w\- ]+?)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)\s+([\d:\.]+)\s+(\d+)\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' (\d+)\s+(\d+)\s+(\w+)\s+(\w+)\s+(\w{3})\s+([\d:\.]+)\s
        ' (\d+)\s+(\d+)\s+(\w+)\s+(\w+|[a-z]+-[a-z]+)\s+(\w{3})\s+(\d{2}\.)(\d{2})\s+
        ' \s(\w{3})\s+((\d{2}\.)(\d{2}))\s+
        ' \s(\w{3})\s+((\d{2}\.\d{2}))\s+
        Dim first As TimeSpan = Nothing
        Dim i As Integer
        While m.Success

            If Debug = True Then
                For i = 1 To m.Groups.Count - 1
                    WriteLog(logFolder, "output of " & i.ToString() & ": " & m.Groups(i).Value.ToString())
                Next
            End If

            Dim rank As String = ""
            Dim bib As String = ""
            Dim name As String = ""
            Dim nation As String = ""
            Dim strTime As String = ""
            Dim strDiff As String = ""
            Dim firstname As String = ""
            Dim lastname As String = ""

            rank = m.Groups(1).Value
            bib = m.Groups(2).Value
            ' Dim name As String = m.Groups(3).Value
            ' Dim nation As String = m.Groups(4).Value

            firstname = m.Groups(3).Value.Trim().Replace(vbCrLf, "")
            lastname = m.Groups(4).Value.Trim().Replace(vbCrLf, "")


            nation = m.Groups(5).Value.ToString() ' endret fra 4 til 18
            strTime = m.Groups(6).Value.ToString()

            If nation = "NED" Then
                ' We shall change the name based on some rules
                Dim lastnameNederland As Array = lastname.Split(" ")

                ' empty lastname string
                lastname = ""
                lastnameNederland(UBound(lastnameNederland)) = StrConv(lastnameNederland(UBound(lastnameNederland)), VbStrConv.ProperCase)

                Dim forCount As Integer = 0

                For forCount = 0 To UBound(lastnameNederland)
                    lastname &= lastnameNederland(forCount) & " "
                Next
                lastname = lastname.Trim()
                WriteLog(logFolder, "Lastname: " & lastname)

                name = StrConv(firstname, VbStrConv.ProperCase) & " " & lastname


            Else
                name = firstname & " " & lastname
                name = StrConv(name, VbStrConv.ProperCase)
            End If

            If Debug = True Then
                WriteLog(logFolder, "Data vi har: " & rank & " " & bib & " " & name & " " & nation)
            End If


            'Fetch the time data
            ' this part here is no longer needed as ISU provides diffs and more in their results
            'Dim time, diff As TimeSpan
            'If m.Groups(7).Value = "" Then
            '    ' first = GetTimeSpan(m.Groups(5).Value, 10) ' Endret fra 10 til 5
            '    first = GetTimeSpan(m.Groups(6).Value, 10) ' Endret fra 5 til 6
            '    time = first
            'Else
            '    diff = GetTimeSpan(m.Groups(7).Value, 10) ' Endret fra 10 til 5
            '    time = first.Add(diff)
            'End If

            'Format times

            'strTime = ConvertTools.FormatTimeSkate(time, 10) ' Endret fra 10 til 5
            'strDiff = ConvertTools.FormatTimeSkate(diff, 10) ' Endret fra 10 til 5

            'strDiff = m.Groups(7).Value

            strTime = strTime.Replace(".", ",").ToString()
            strTime = strTime.Replace(":", ".").ToString()
            strDiff = ""

            'If strTime <> "" Then
            '    Dim t, d As TimeSpan
            '    If rank = 1 Then
            '        first = GetTimeSpan(strTime)
            '        t = first
            '        d = New TimeSpan(0)
            '    Else
            '        t = GetTimeSpan(strTime)
            '        d = t.Subtract(first)
            '    End If

            '    strTime = ConvertTools.FormatTime(t)
            '    strDiff = ConvertTools.FormatTime(d)
            'End If


            Dim athlete As New athlete(rank, bib, name, nation, strTime, strDiff)

            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        Return athlete_list.Count
    End Function

    'Returns a formated resultlist, for individual starts
    Public Function SCG_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String,
                                ByRef job As MainConvertModule.FolderJob) As String
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.SCG_General")
        'Fill the athlete list
        Dim c As Integer = 0
        If format = MainConvertModule.FormatType.SpeedSkatingSprint Then
            c = ParseDataSCGSprint(filename, job)
        ElseIf format = MainConvertModule.FormatType.SpeedSkatingTeamEvent Then
            c = ParseDataSCGRelay(filename, job)
        ElseIf format = MainConvertModule.FormatType.SpeedSkatingNormal Then
            c = ParseDataSCG(filename, job)
        End If

        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete

        For Each at In athlete_list

            'Add paragraphs
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.SpeedSkatingSprint
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & ", "
                Case MainConvertModule.FormatType.IndividualStart
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                Case MainConvertModule.FormatType.SpeedSkatingTeamEvent
                    ret &= at.rank & ") " & at.nation & " " & at.time & " " & at.name & ", "
                Case Else
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    End If
                    '    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.time & "), "
                    'Case Else
                    '    If i = 1 Then
                    '        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.time & "), "
                    '    ElseIf i = 2 Then
                    '        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " min bak (" & at.time & "), "
                    '    Else
                    '        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.time & "), "
                    '    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function

    'Takes a speedskating HTML list and fills the athlete list
    Protected Function ParseDataSCGRelay(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.ParseDataSCG")

        Dim e As Encoding
        e = Encoding.GetEncoding(job.FileEncoding.ToUpper())
        If job.FileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If

        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        ' Remove span
        ' 
        If Debug = True Then WriteLog(logFolder, "Remove tags")
        Dim span As Regex = New Regex("<span\b[^>]*>(\d)+(.*?)</span>", RegexOptions.Compiled) 'Drop all tags
        content = span.Replace(content, " ")
        If Debug = True Then
            WriteLog(logFolder, content)
        End If


        'Remove tags
        If Debug = True Then
            WriteLog(logFolder, "Removing more tags")
        End If
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, " ")

        If Debug = True Then
            WriteLog(logFolder, content)

        End If
        'Split off header
        If Debug = True Then WriteLog(logFolder, "Split off header")
        Dim topsplit As String = "Rankings"
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results
        If Debug = True Then WriteLog(logFolder, "Find results with RegExp")
        ' Dim res As Regex = New Regex("(\d+)\s+(\d+)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)(\(\d\))?\s+([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' Dim res As Regex = New Regex("(\d+)\s+(\d+)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)(\(\d\))?\s+([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        ' Dim res As Regex = New Regex("([0-9]+)\s+([0-9]+)\s+([a-z-����������]+\s+)\s+([a-z]{3})\s+([0-9]+:[0-9]+\.[0-9]+|[0-9]+\.[0-9]+|[0-9]+:[0-9]+\.[0-9]+)\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' Dim res As Regex = New Regex("([0-9]+)\s+([0-9]+)\s+(([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+)\s+(([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+([a-z-����������]+)\s+|([a-z-����������]+)\s+([a-z-����������]+)\s+([a-z-����������]+)\s+([a-z-����������]+)\s+)\s+([a-z]{3})\s+([0-9]+:[0-9]+\.[0-9]+|[0-9]+\.[0-9]+|[0-9]+:[0-9]+\.[0-9]+)\s+", _
        '                             RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        ' Finding Team Tempo

        Dim res As Regex = New Regex(job.Regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        Dim m As Match = res.Match(content)

        If Not m.Success Then
            WriteLog(logFolder, "Regex returned no matches....")
        End If

        Dim first As TimeSpan = Nothing
        Dim i As Integer

        While m.Success

            If Debug = True Then
                For i = 1 To m.Groups.Count - 1
                    WriteLog(logFolder, "output of " & i.ToString() & ": " & m.Groups(i).Value.ToString())
                Next
            End If

            Dim rank As String = m.Groups(1).Value
            Dim bib As String = m.Groups(2).Value

            Dim name As String = ""
            Dim nation As String = ""

            Dim strTime As String = ""
            Dim strDiff As String = ""

            nation = m.Groups(8).Value.ToString()
            Dim strAthleteOne As String
            Dim strAthleteTwo As String
            Dim strAthleteThree As String
            Dim strAthleteFour As String

            If m.Groups(4).Value.ToString() <> String.Empty Then
                strAthleteOne = StrConv(m.Groups(4).Value.ToString(), vbProperCase)
            Else
                strAthleteOne = StrConv(m.Groups(8).Value.ToString(), vbProperCase)
            End If

            If m.Groups(5).Value.ToString() <> String.Empty Then
                strAthleteTwo = StrConv(m.Groups(5).Value.ToString(), vbProperCase)
            Else
                strAthleteTwo = StrConv(m.Groups(9).Value.ToString(), vbProperCase)
            End If

            If m.Groups(6).Value.ToString() <> String.Empty Then
                strAthleteThree = StrConv(m.Groups(6).Value.ToString(), vbProperCase)
            Else
                strAthleteThree = StrConv(m.Groups(10).Value.ToString(), vbProperCase)
            End If

            If m.Groups(7).Value.ToString() <> String.Empty Then
                strAthleteFour = StrConv(m.Groups(7).Value.ToString(), vbProperCase)
            Else
                strAthleteFour = String.Empty
            End If
            
            name = "(" & strAthleteOne & ", " & strAthleteTwo & ", " & strAthleteThree & ")"
            If strAthleteFour <> String.Empty Then
                name = "(" & strAthleteOne & ", " & strAthleteTwo & ", " & strAthleteThree & ", " & strAthleteFour + ")"
            End If

            If Debug = True Then
                WriteLog(logFolder, "Data vi har: " & rank & " " & bib & " " & name & " " & nation)
            End If

            strTime = m.Groups(9).Value.ToString()
            strTime = strTime.Replace(".", ",").ToString()
            strTime = strTime.Replace(":", ".").ToString()

            If strTime <> "" Then
                Dim t, d As TimeSpan
                If rank = 1 Then
                    first = GetTimeSpan(strTime, 1)
                    t = first
                    d = New TimeSpan(0)
                Else
                    t = GetTimeSpan(strTime, 1)
                    d = t.Subtract(first)
                End If

                strTime = ConvertTools.FormatTime(t, 1)
                strDiff = ConvertTools.FormatTime(d, 1)
            End If

            Dim athlete As New athlete(rank, bib, name, nation, strTime, strDiff)

            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        Return athlete_list.Count
    End Function


End Class
