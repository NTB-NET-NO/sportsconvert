Imports System.IO
Imports System.xml
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports SportsConvert.MainConvertModule

Imports ntb_FuncLib.LogFile

Public Class FIS_Live

    Public ErrorFolder As String

    Protected Class athlete

        'General
        Public rank As String
        Public bib As String
        Public name As String
        Public nat_code As String
        Public nation As String



        'Cross country specifics
        Public start As String
        Public time As String
        Public diff As String
        Public status As String

        'Ski jumping specifics
        Public distance_1 As String
        Public distance_2 As String
        Public points_1 As String
        Public points_2 As String
        Public rank_1 As String
        Public rank_2 As String

        'Alpine specifics
        Public time2 As String
        Public total As String

        Public points_total As String

        'Constructors

        'Cross country
        ' This might have to be changed a bit if we want to do relay here as well... 
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal start_in As String, ByVal time_in As String, ByVal diff_in As String, ByVal status_in As String)
            Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in

            If MainConvertModule.FormatType.Relay = FormatType.Relay Then
                nation = MainConvertModule.NationMap(nat_code.Substring(0, 3))
                nation &= nat_code.Substring(3)
            Else
                nation = MainConvertModule.NationMap(nat_code)
            End If

            If nation = "" Then nation = natcode_in

            'This is cross country
            start = start_in
            time = time_in
            diff = diff_in
            status = status_in

            ' This check is here to make sure that we are not rotating a name when we are working with relay-results
            If Not MainConvertModule.FormatType.Relay = FormatType.Relay Then
                name = ConvertTools.RotateAndFixName(name)
            End If

            'Character map substitutions

            Dim subst As String() = character_map(nat_code)
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
            End If


        End Sub
        'Alpine
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal time_in As String, ByVal time2_in As String, ByVal total_in As String)
            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in
            'This is Alpine
            time = time_in
            time2 = time2_in
            total = total_in


            nation = MainConvertModule.NationMap(nat_code)
            If nation = "" Then nation = natcode_in



            'This is cross country
            ' start = start_in
            'time = time_in
            ' diff = diff_in
            'status = status_in

            name = ConvertTools.RotateAndFixName(name)

            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
            End If

        End Sub
        'Ski jumping
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, _
                       ByVal distance_1_in As String, ByVal distance_2_in As String, ByVal points_1_in As String, ByVal points_2_in As String, _
                       ByVal rank_1_in As String, ByVal rank_2_in As String, ByVal points_total_in As String)
            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in

            nation = MainConvertModule.NationMap(nat_code)
            If nation = "" Then nation = natcode_in

            name = ConvertTools.RotateAndFixName(name, ConvertTools.NameFormat.UCASE)

            'This is ski jumping
            distance_1 = distance_1_in
            distance_2 = distance_2_in
            points_1 = points_1_in
            points_2 = points_2_in
            rank_1 = rank_1_in
            rank_2 = rank_2_in

            points_total = points_total_in

            distance_1 = distance_1.Replace(".", ",").Replace("m", "").Replace(",0", "")
            distance_2 = distance_2.Replace(".", ",").Replace("m", "").Replace(",0", "")

            points_1 = points_1.Replace(".", ",")
            points_2 = points_2.Replace(".", ",")
            points_total = points_total.Replace(".", ",")

            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
            End If

        End Sub

    End Class

    Protected athlete_list As ArrayList

    Protected Shared character_map As New Hashtable
    'Object constructor creates object and loads mapping lists
    Public Sub New()

        athlete_list = New ArrayList

        'Load character map
        Dim charmapfile As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("CharacterMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(charmapfile)

            Dim nodes As XmlNodeList
            Dim nd1, nd2 As XmlNode
            nodes = map.SelectNodes("/character-maps/nation")

            'Fill hash
            For Each nd1 In nodes
                If nd1.ChildNodes().Count > 0 Then

                    Dim c As Integer = 0
                    Dim charset((nd1.ChildNodes().Count * 2) - 1) As String
                    For Each nd2 In nd1.ChildNodes()
                        charset(c) = nd2.Attributes("value").Value
                        charset(c + 1) = nd2.Attributes("replace").Value
                        c += 2
                    Next

                    character_map(nd1.Attributes("code").Value) = charset
                End If
            Next

            WriteLog(New String(ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))), "FIS - Character conversion map loaded (" & charmapfile & "): " & character_map.Count & " conversion sets.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(New String(ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))), "Error loading charachter conversion map (" & charmapfile & ") for FIS", ex)

            Throw New Exception(ex.Message)
        End Try

    End Sub
    'Takes a cross country HTML list and fills the athlete list
    Protected Function ParseDataCCRelay(ByVal filename As String, ByVal format As MainConvertModule.FormatType,
                                        ByVal job As MainConvertModule.FolderJob) As Integer

        Dim e As Encoding
        e = Encoding.GetEncoding(job.fileEncoding)
        If job.fileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If

        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, "")
        content = content.Replace("&nbsp;", " ")

        'Split off header
        Dim topsplit As String = "qualified for final A / B"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "FIS Points"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbLf & "Result"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbCrLf & "Result"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Try primary data scource parse

        ' The line below grabs the relay results, but only the country. 
        ' &nbsp;(\d+)\s+&nbsp;\s+([\w\- \/\.]+)&nbsp;\s+&nbsp;\s+(\w{3})\s+([\d:\.]+)\s+&nbsp;\s+&nbsp;\s+
        Dim res As Regex

        ' res = New Regex("&nbsp;(\d+)\s+&nbsp;\s+([\w\- \/\.]+)&nbsp;\s+&nbsp;\s+(\w{3})\s+([\d:\.]+)\s+&nbsp;\s+&nbsp;\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        ' The above line is marked out as it does not work as of 25.11.2010.
        ' 
        ' This works in sprint:
        ' ([0-9]{1,3})\s+([0-9]{1,3})\s+([0-9]+)\s+((([A-Z]+)\s+([A-Z]{1,3})\s+)|(([A-Z]+)\s+))\s+([A-Z]{3})\s+((([0-9])|([0-9]{2})):([0-9]{2})\.(([0-9]{1})|([0-9]{2})))\s+
        ' 
        res = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim m As Match = res.Match(content)

        ' ([A-Z]{1,3})|([A-Z]+)\s+([A-Z]{3})\s+([0-9]{1}:[0-9]{2}:[0-9]{2}\.[0-9]{1}|[0-9]{2}:[0-9]{2}\.[0-9]{1})\s+(^.*$)\s+(^.*$)\s+\s+(^.*$)\s+(^.*$)\s+(^.*$)\s+(^.*$)\s+(^.*$)\s+(^.*$)\s+\n+
        ' If the match result in no success, this means that we are dealing with a relay in Cross Country
        If Not m.Success Then
            ' Not sure what to do here
        End If

        Dim rank As String = ""
        Dim bib As String = ""
        Dim name As String = ""
        Dim nation As String = ""
        Dim start As String = ""
        Dim time As String = ""
        Dim diff As String = ""
        Dim status As String = ""

        While m.Success

            ' Added support for relay team members 29.12.2010
            Dim strRelayAthleteOne As String = ""
            Dim strRelayAthleteTwo As String = ""
            Dim strRelayAthleteThree As String = ""
            Dim strRelayAthleteFour As String = ""
            Dim strRelayAthleteOneTime As String = ""
            Dim strRelayAthleteTwoTime As String = ""
            Dim strRelayAthleteThreeTime As String = ""
            Dim strRelayAthleteFourTime As String = ""

            Dim iCounter As Integer
            Dim counter As Integer = m.Groups.Count - 1
            Dim boolFoo As Boolean = True
            If boolFoo = True Then
                For iCounter = 1 To counter

                    If Not m.Groups(iCounter).Value = Nothing Then
                        WriteLog(logFolder, "output of " & iCounter.ToString() & ": " & m.Groups(iCounter).Value.ToString())
                    End If
                Next

            End If

            Try
                ' Some stuff is equal for both relays

                ' We are setting the rank
                rank = m.Groups(1).Value

                ' We are setting the name of the team
                ' name = m.Groups(2).Value.ToString()
                name = ""

                ' We are setting the bib for the athlete, in this case to nothing
                bib = Nothing

                ' If we are working on the main relay with team members we are doing this here



                ' If we are working on the sprint relay, we are doing this here

                nation = m.Groups(5).Value

                If m.Groups(3).Value.Trim().ToString() <> "" Then
                    If m.Groups(3).Value.Length <= 3 Then
                        nation &= " " & m.Groups(3).Value.ToString()
                    End If

                End If
                time = m.Groups(6).Value.ToString()
                time = time.Replace(".", ",")
                time = time.Replace(":", ".")


                diff = time

                start = Nothing ' m.Groups(6).Value
                ' name = m.Groups(2).Value.ToString()
                name = " "
                status = ""

                ' Now getting the team members
                strRelayAthleteOne = Replace(m.Groups(7).Value, "&nbsp;", "").ToString()
                strRelayAthleteOne = ConvertTools.RotateFISName(strRelayAthleteOne).ToString()
                strRelayAthleteOneTime = Replace(m.Groups(8).Value, "&nbsp;", "").ToString()
                strRelayAthleteOneTime = Replace(strRelayAthleteOneTime, ".", ",")
                strRelayAthleteOneTime = Replace(strRelayAthleteOneTime, ":", ".")

                strRelayAthleteTwo = Replace(m.Groups(9).Value, "&nbsp;", "").ToString()
                strRelayAthleteTwo = ConvertTools.RotateFISName(strRelayAthleteTwo).ToString()
                strRelayAthleteTwoTime = Replace(m.Groups(10).Value, "&nbsp;", "").ToString()
                strRelayAthleteTwoTime = Replace(strRelayAthleteTwoTime, ".", ",")
                strRelayAthleteTwoTime = Replace(strRelayAthleteTwoTime, ":", ".")

                strRelayAthleteThree = Replace(m.Groups(11).Value, "&nbsp;", "").ToString()
                strRelayAthleteThree = ConvertTools.RotateFISName(strRelayAthleteThree).ToString()
                strRelayAthleteThreeTime = Replace(m.Groups(12).Value, "&nbsp;", "").ToString()
                strRelayAthleteThreeTime = Replace(strRelayAthleteThreeTime, ".", ",")
                strRelayAthleteThreeTime = Replace(strRelayAthleteThreeTime, ":", ".")

                strRelayAthleteFour = Replace(m.Groups(13).Value, "&nbsp;", "").ToString()
                strRelayAthleteFour = ConvertTools.RotateFISName(strRelayAthleteFour).ToString()
                strRelayAthleteFourTime = Replace(m.Groups(14).Value, "&nbsp;", "").ToString()
                strRelayAthleteFourTime = Replace(strRelayAthleteFourTime, ".", ",")
                strRelayAthleteFourTime = Replace(strRelayAthleteFourTime, ":", ".")

                ' Creating the list of names for the relay team
                name = "(" & _
                    strRelayAthleteOne & " " & strRelayAthleteOneTime & ", " & _
                    strRelayAthleteTwo & " " & strRelayAthleteTwoTime & ", " & _
                    strRelayAthleteThree & " " & strRelayAthleteThreeTime & ", " & _
                    strRelayAthleteFour & " " & strRelayAthleteFourTime & ")"




                'Format the times
                If Not time.Trim().ToString() = "Semifinale" Then
                    Dim first As TimeSpan
                    'Format the times
                    If time <> "" Then
                        Dim t, d As TimeSpan
                        If rank = 1 Then
                            first = GetTimeSpan(time)
                            t = first
                            d = New TimeSpan(0)
                        Else
                            t = GetTimeSpan(time)
                            d = t.Subtract(first)
                        End If

                        time = ConvertTools.FormatTime(t)
                        diff = ConvertTools.FormatTime(d)
                    End If
                    'Dim span As TimeSpan = ConvertTools.GetTimeSpan(time)
                    'time = ConvertTools.FormatTime(span)

                    'span = ConvertTools.GetTimeSpan(diff)
                    'diff = ConvertTools.FormatTime(span)
                Else
                    time = ""
                End If


                Dim athlete As New athlete(rank, bib, name, nation, start, time, diff, status)
                athlete_list.Add(athlete)

                m = m.NextMatch()

                ' This line is here in case we get 1.02.53,8
                status = Nothing ' status = m.Groups(9).Value
            Catch nex As NullReferenceException
                WriteLog(logFolder, nex.ToString())
                WriteLog(logFolder, nex.Message.ToString())

                Throw New Exception(nex.Message)
            Catch nnex As NoNullAllowedException
                WriteLog(logFolder, nnex.ToString())
                WriteLog(logFolder, nnex.Message.ToString())

                Throw New Exception(nnex.Message)
            Catch ex As Exception
                WriteLog(logFolder, ex.ToString())
                WriteLog(logFolder, ex.Message.ToString())

                Throw New Exception(ex.Message)
            End Try

        End While



        Return athlete_list.Count


    End Function

    'Takes a cross country HTML list and fills the athlete list
    Protected Function ParseDataCcSprintRelay(ByVal filename As String, ByVal format As MainConvertModule.FormatType,
                                              ByVal job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))

        Dim e As Encoding
        e = Encoding.GetEncoding(job.FileEncoding)
        If job.FileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If


        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, "")
        content = content.Replace("&nbsp;", " ")

        WriteLog(logFolder, content)
        'Split off header
        Dim topsplit As String = "qualified for final A / B"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "FIS Points"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbLf & "Result"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbCrLf & "Result"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Try primary data scource parse

        ' The line below grabs the relay results, but only the country. 
        ' &nbsp;(\d+)\s+&nbsp;\s+([\w\- \/\.]+)&nbsp;\s+&nbsp;\s+(\w{3})\s+([\d:\.]+)\s+&nbsp;\s+&nbsp;\s+
        Dim res As Regex
        Dim sprintRelay As Boolean = False
        Dim sprintRelayWithNames As Boolean = True
        res = New Regex(job.Regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim m As Match = res.Match(content)
        If Not m.Success Then
            res = New Regex("([0-9]{1,3})\s+([A-Z]+)\s+(\s+|[A-Z]{1,3}|[A-Z]+)\s+([A-Z]{3})\s+([0-9]{2}:[0-9]{2}\.[0-9]{1})\s+(.*[^\|0-9+])\s+(.*[^\|0-9+])\s+\n+", _
                    RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

            m = res.Match(content)
            ' This can mean that we have relay results without team members. 
        Else
            sprintRelayWithNames = False
        End If

        Dim rank As String = ""
        Dim bib As String = ""
        Dim name As String = ""
        Dim nation As String = ""
        Dim start As String = ""
        Dim time As String = ""
        Dim diff As String = ""
        Dim status As String = ""

        While m.Success

            ' Added support for relay team members 29.12.2010
            Dim strRelayAthleteOne As String = ""
            Dim strRelayAthleteTwo As String = ""
            Dim strRelayAthleteOneTime As String = ""
            Dim strRelayAthleteTwoTime As String = ""


            Dim counter As Integer = m.Groups.Count - 1
            Dim boolFoo As Boolean = True

            Try
                ' Some stuff is equal for both relays

                ' We are setting the rank
                rank = m.Groups(1).Value

                ' We are setting the name of the team
                ' name = m.Groups(2).Value.ToString()
                name = ""

                ' We are setting the bib for the athlete, in this case to nothing
                bib = Nothing

                ' If we are working on the main relay with team members we are doing this here


                If sprintRelayWithNames = True Then
                    nation = m.Groups(4).Value

                    ' If we have Country III Then we add to NOR 
                    If m.Groups(3).Value.ToString() <> "" Then

                        If m.Groups(3).Value.Length <= 3 Then
                            nation &= " " & m.Groups(3).Value.ToString()
                        End If

                    End If

                    time = m.Groups(5).Value.ToString()
                    diff = time

                    ' Now getting the team members
                    strRelayAthleteOne = Replace(m.Groups(6).Value, "&nbsp;", "").ToString()
                    strRelayAthleteOne = ConvertTools.RotateFISName(strRelayAthleteOne).ToString()

                    strRelayAthleteTwo = Replace(m.Groups(7).Value, "&nbsp;", "").ToString()
                    strRelayAthleteTwo = ConvertTools.RotateFISName(strRelayAthleteTwo).ToString()

                    ' Creating the list of names for the relay team
                    name = "(" & _
                        strRelayAthleteOne & ", " & _
                        strRelayAthleteTwo & ")"
                ElseIf sprintRelayWithNames = False Then
                    time = m.Groups(7).Value.Trim().ToString()
                    nation = m.Groups(6).Value.Trim().ToString()

                    ' If we have Country III Then we add to NOR 
                    If m.Groups(5).Value.ToString() <> "" Then

                        If m.Groups(5).Value.Length <= 3 Then
                            nation &= " " & m.Groups(5).Value.ToString()
                        End If

                    End If

                End If



                'Format the times
                If Not time.Trim().ToString() = "Semifinale" Then
                    Dim first As TimeSpan
                    'Format the times
                    If time <> "" Then
                        Dim t, d As TimeSpan
                        If rank = 1 Then
                            first = GetTimeSpan(time)
                            t = first
                            d = New TimeSpan(0)
                        Else
                            t = GetTimeSpan(time)
                            d = t.Subtract(first)
                        End If

                        time = ConvertTools.FormatTime(t)
                        diff = ConvertTools.FormatTime(d)
                    End If
                    'Dim span As TimeSpan = ConvertTools.GetTimeSpan(time)
                    'time = ConvertTools.FormatTime(span)

                    'span = ConvertTools.GetTimeSpan(diff)
                    'diff = ConvertTools.FormatTime(span)
                Else
                    time = ""
                End If


                Dim athlete As New athlete(rank, bib, name, nation, start, time, diff, status)
                athlete_list.Add(athlete)

                m = m.NextMatch()

                ' This line is here in case we get 1.02.53,8
                status = Nothing ' status = m.Groups(9).Value
            Catch nex As NullReferenceException
                WriteLog(logFolder, nex.ToString())
                WriteLog(logFolder, nex.Message.ToString())

                Throw New Exception(nex.Message)
            Catch nnex As NoNullAllowedException
                WriteLog(logFolder, nnex.ToString())
                WriteLog(logFolder, nnex.Message.ToString())

                Throw New Exception(nnex.Message)
            Catch ex As Exception
                WriteLog(logFolder, ex.ToString())
                WriteLog(logFolder, ex.Message.ToString())

                ' We must move the file to error folder
                ' File.Copy(filename, ErrorFolder & "\" & Path.GetFileNameWithoutExtension(filename) & "-" & File.GetCreationTime(filename).ToString("yyyyMMdd-HHmmss") & Path.GetExtension(filename), True)
                ' File.Delete(filename)
                Throw New Exception(ex.Message)
            End Try

        End While



        Return athlete_list.Count


    End Function


    Protected Function ParseDataCC(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))

        Dim e As Encoding
        e = Encoding.GetEncoding(job.fileEncoding)
        If job.fileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If


        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, "")
        content = content.Replace("&nbsp;", " ")

        'Split off header
        Dim topsplit As String = "qualified for final A / B"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "FIS Points"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbLf & "Result"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbCrLf & "Result"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        WriteLog(logFolder, content)
        'Try primary data scource parse

        ' The line below grabs the relay results, but only the country. 
        ' &nbsp;(\d+)\s+&nbsp;\s+([\w\- \/\.]+)&nbsp;\s+&nbsp;\s+(\w{3})\s+([\d:\.]+)\s+&nbsp;\s+&nbsp;\s+
        Dim res As Regex
        Dim boolSprint As Boolean = False

        ' This works with relay
        ' &nbsp;([0-9]{1}|[0-9]{2})\s+(([A-Z]+)\s+([A-Z]{1,3})|([A-Z]+))&nbsp;\s+&nbsp;\s+&nbsp;\s+([A-Z]{3})\s+(([0-9]{1}:[0-9]{2}:[0-9]{2}\.[0-9]{1})|([0-9]{2}:[0-9]{2}\.[0-9]{1}))\s+&nbsp;\s+&nbsp;\s+(^.*$)\s+&nbsp;\s+(^.*$)\s+&nbsp;\s+&nbsp;\s+(^.*$)\s+&nbsp;\s+(^.*$)\s+&nbsp;\s+&nbsp;\s+(^.*$)\s+&nbsp;\s+(^.*$)\s+&nbsp;\s+&nbsp;\s+(^.*$)\s+&nbsp;\s+(^.*$)\s+
        ' This is an old one
        ' &nbsp;([0-9]{1}|[0-9]{2})\s+(([A-Z]+)\s+([A-Z]{1,3})|([A-Z]+))&nbsp;\s+&nbsp;\s+&nbsp;\s+([A-Z]{3})\s+(([0-9]{1}:[0-9]{2}:[0-9]{2}\.[0-9]{1})|([0-9]{2}:[0-9]{2}\.[0-9]{1}))\s+&nbsp;\s+&nbsp;

        res = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        'Find results 1

        ' Dim res As Regex = New Regex("&nbsp;(\d+|\w{3})&nbsp;\s+(\d+)&nbsp;\s+&nbsp;([\w\- \/\.&;]+?)(&nbsp;)?\s+&nbsp;(\w{3})\s+&nbsp;\s+([*\d :]+)&nbsp;\s+([\d:\.]*)&nbsp;\s+([\d:\.\+]*)&nbsp;\s+&nbsp;(\w*)\s*", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim m As Match = res.Match(content)

        'Find results 2
        If Not m.Success Then
            boolSprint = True
            ' Changing this to be used in sprint events
            res = New Regex("([0-9]{1,3})\s+([0-9]{1,3})\s+([0-9]{1,8})\s+(.*[^\s+0-9])\s+[0-9]{4}\s+([A-Z]{3})\s+\n+", _
            RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

            ' res = New Regex("(\d+|\w{3})\s+(\d+)\s+([\w\- \/\.&;]+?)\s+(&nbsp;)?(\w{3})\s+([*\d :]+)\s+([\d:\.]*)\s+([\d:\.\+]*)", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

            m = res.Match(content)
        End If


        While m.Success

            Dim rank As String = ""
            Dim bib As String = ""
            Dim name As String = ""
            Dim nation As String = ""
            Dim start As String = ""
            Dim time As String = ""
            Dim diff As String = ""
            Dim status As String = ""




            Dim iCounter As Integer
            Dim counter As Integer = m.Groups.Count - 1
            Dim boolFoo As Boolean = True
            Try
                If boolFoo = True Then
                    For iCounter = 1 To counter

                        If Not m.Groups(iCounter).Value = Nothing Then
                            WriteLog(logFolder, "output of " & iCounter.ToString() & ": " & m.Groups(iCounter).Value.ToString())
                        End If
                    Next

                End If



                rank = m.Groups(1).Value
                bib = m.Groups(2).Value
                name = m.Groups(4).Value.ToString()
                name = ConvertTools.RotateFISName(name).ToString()
                ' name = ConvertTools.RotateAndFixName(name, NameFormat.BOTH, True).ToString()
                nation = m.Groups(5).Value
                If boolSprint = False Then
                    time = m.Groups(6).Value
                    time = time.Replace(".", ",")
                    time = time.Replace(":", ".")
                End If

                diff = time

                start = Nothing ' m.Groups(6).Value
                ' name = m.Groups(2).Value.ToString()
                ' This line is here in case we get 1.02.53,8
                status = Nothing ' status = m.Groups(9).Value
            Catch nex As NullReferenceException
                WriteLog(logFolder, nex.ToString())
                WriteLog(logFolder, nex.Message.ToString())

                Throw New Exception(nex.Message)
            Catch nnex As NoNullAllowedException
                WriteLog(logFolder, nnex.ToString())
                WriteLog(logFolder, nnex.Message.ToString())

                Throw New Exception(nnex.Message)
            Catch ex As Exception
                WriteLog(logFolder, ex.ToString())
                WriteLog(logFolder, ex.Message.ToString())

                Throw New Exception(ex.Message)
            End Try





            'Format the times
            If boolSprint = False Then
                If Not time.Trim().ToString() = "Semifinale" Then
                    Dim first As TimeSpan
                    'Format the times
                    If time <> "" Then
                        Dim t, d As TimeSpan
                        If rank = 1 Then
                            first = GetTimeSpan(time)
                            t = first
                            d = New TimeSpan(0)
                        Else
                            t = GetTimeSpan(time)
                            d = t.Subtract(first)
                        End If

                        time = ConvertTools.FormatTime(t)
                        diff = ConvertTools.FormatTime(d)
                    End If
                    'Dim span As TimeSpan = ConvertTools.GetTimeSpan(time)
                    'time = ConvertTools.FormatTime(span)

                    'span = ConvertTools.GetTimeSpan(diff)
                    'diff = ConvertTools.FormatTime(span)
                Else
                    time = ""
                End If
            End If


            Dim athlete As New athlete(rank, bib, name, nation, start, time, diff, status)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        'If none found try secondary
        If athlete_list.Count = 0 Then

            Dim first As TimeSpan
            If Debug = True Then
                WriteLog(logFolder, content)
                WriteLog(logFolder, "We are trying secondary because we haven't found anything")
            End If
            res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?", _
                            RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            ' res = New Regex("&nbsp;([0-9]+)\s+([a-z]+)\s*([a-z]+)*&nbsp;\s+&nbsp;\s+&nbsp;\s+([a-z]{3})\s+([0-9]{1}|[0-9]{2}):([0-9]{1}|[0-9]{2})\.([0-9]{1}|[0-9]{2})\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            m = res.Match(content)
            While m.Success

                Dim rank As String = m.Groups(1).Value
                Dim bib As String = m.Groups(2).Value
                Dim name As String = m.Groups(4).Value
                name = ConvertTools.RotateFISName(name)
                Dim nation As String = m.Groups(6).Value
                Dim start As String = ""
                Dim time As String = m.Groups(7).Value
                Dim diff As String = ""
                Dim status As String = ""

                'Format the times
                If time <> "" Then
                    Dim t, d As TimeSpan
                    If rank = 1 Then
                        first = GetTimeSpan(time)
                        t = first
                        d = New TimeSpan(0)
                    Else
                        t = GetTimeSpan(time)
                        d = t.Subtract(first)
                    End If

                    time = ConvertTools.FormatTime(t)
                    diff = ConvertTools.FormatTime(d)
                End If

                Dim athlete As New athlete(rank, bib, name, nation, start, time, diff, status)
                athlete_list.Add(athlete)

                m = m.NextMatch()
            End While

        End If

        Return athlete_list.Count
    End Function

    Public Function KO_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String,
                               ByRef job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        'Fill the athlete list
        Dim c As Integer = ParseDataKO(filename, format, job)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.KombineretNormal
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " min bak, "
                    ElseIf i > 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & ", "
                    End If
                Case MainConvertModule.FormatType.KombinertStafett
                    If i = 1 Then
                        ret &= at.rank & ") " & at.nation & " " & at.time & " " & at.name & ", "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.nation & " " & at.diff & " min bak " & at.name & ", "
                    ElseIf i > 2 Then
                        ret &= at.rank & ") " & at.nation & " " & at.diff & " " & at.name & ", "
                    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        ret = ret.Replace("  ", " ")
        body = ret

        Return c

    End Function

    Protected Function ParseDataKO(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByVal job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim input As StreamReader = New StreamReader(filename, Encoding.GetEncoding(job.fileEncoding))
        Dim debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))

        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        WriteLog(logFolder, "Converting Nordic Combined")


        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, " ")
        content = content.Replace("&nbsp;", " ")

        'Split off header
        Dim topsplit As String = "qualified for final A / B"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "FIS Points"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbLf & "Result"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbCrLf & "Result"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Try primary data scource parse

        ' The line below grabs the relay results, but only the country. 
        ' &nbsp;(\d+)\s+&nbsp;\s+([\w\- \/\.]+)&nbsp;\s+&nbsp;\s+(\w{3})\s+([\d:\.]+)\s+&nbsp;\s+&nbsp;\s+
        Dim res As Regex = New Regex("")
        Dim boolRelay As Boolean = False
        Dim sel As Integer = 1
        Dim m As Match = Nothing

        Dim CombinedTeam As Boolean = True
        'WriteLog(logFolder, content)
        'WriteLog(logFolder, "We are trying Nordic Combined because this matched")

        ' Need to find out why this one does not give the planned result
        If format = FormatType.KombinertStafett Then
            ' WriteLog(logFolder, "Running regex for team event")
            res = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            m = res.Match(content)
        ElseIf format = FormatType.KombineretNormal Then
            res = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            m = res.Match(content)
            CombinedTeam = False
        End If
        m = res.Match(content)
        While m.Success
            'For i As Integer = 0 To 7
            '    WriteLog(logFolder, "Value of m.groups(" & i & "): " & m.Groups(i).Value.ToString())
            'Next

            Dim time As String = ""
            Dim diff As String = ""
            Dim start As String = ""
            Dim status As String = ""
            Dim rank As String = ""
            Dim bib As String = ""
            Dim name As String = ""
            Dim nation As String = ""
            Dim first As TimeSpan
            If CombinedTeam = False Then
                rank = m.Groups(1).Value.ToString()
                bib = m.Groups(2).Value.ToString()
                name = m.Groups(4).Value.ToString()
                name = name.Replace(vbCrLf, " ")
                name = name.Replace(vbCr, " ")
                name = ConvertTools.RotateFISName(name)
                nation = m.Groups(5).Value.ToString()
                time = m.Groups(6).Value.ToString()
                time = time.Replace(".", ",")
                time = time.Replace(":", ".")
            ElseIf CombinedTeam = True Then
                WriteLog(logFolder, "Fixing for team event")
                rank = m.Groups(1).Value.ToString()
                Dim name1 As String = m.Groups(5).Value.ToString()
                name1 = name1.Replace(vbCrLf, " ")
                name1 = name1.Replace(vbCr, " ")
                name1 = ConvertTools.RotateFISName(name1)
                Dim name2 As String = m.Groups(6).Value.ToString()
                name2 = name2.Replace(vbCrLf, " ")
                name2 = name2.Replace(vbCr, " ")
                name2 = ConvertTools.RotateFISName(name2)
                Dim name3 As String = m.Groups(7).Value.ToString()
                name3 = name3.Replace(vbCrLf, " ")
                name3 = name3.Replace(vbCr, " ")
                name3 = ConvertTools.RotateFISName(name3)
                Dim name4 As String = m.Groups(8).Value.ToString()
                name4 = name4.Replace(vbCrLf, " ")
                name4 = name4.Replace(vbCr, " ")
                name4 = ConvertTools.RotateFISName(name4)

                name = "(" & name1 & ", " & name2 & ", " & name3 & ", " & name4 & ")"
                nation = m.Groups(3).Value.ToString()
                time = m.Groups(4).Value.ToString()
                time = time.Replace(".", ",")
                time = time.Replace(":", ".")
            End If

            If time <> "" Then
                Dim t, d As TimeSpan
                If rank = 1 Then
                    first = GetTimeSpan(time)
                    t = first
                    d = New TimeSpan(0)
                Else
                    t = GetTimeSpan(time)
                    d = t.Subtract(first)
                End If

                time = ConvertTools.FormatTime(t)
                diff = ConvertTools.FormatTime(d)
            End If



            Dim athlete As New athlete(rank, bib, name, nation, start, time, diff, status)
            athlete_list.Add(athlete)

            m = m.NextMatch()

        End While

        Return athlete_list.Count

    End Function

    Protected Function ParseDataAL(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByVal job As MainConvertModule.FolderJob) As Integer
        ' -----------------------------------------------------------------------------
        ' ParseDataAL
        '
        ' Description:
        '   Takes a alpine HTML list and fills the athlete list
        '   
        ' Parameters:
        '
        ' Returns:
        '
        ' Notes :
        '   Rev 2008-01-14, Trond Hus�: Alpine can consist of two runs, and there are therefor two folders for this.
        '                   in this revision I have changed the code so that it checks which folder the result has been saved in.
        '                   This because the sel-check does not work like it was supposed to. 
        ' -----------------------------------------------------------------------------

        Dim input As StreamReader = New StreamReader(filename, Encoding.GetEncoding(job.fileEncoding))
        ' Dim logFolder As String = My.Settings.LogFolder ' Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))

        Dim content As String = input.ReadToEnd()
        input.Close()


        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, "")
        content = content.Replace("&nbsp;", " ") ' Also removing the space HTML code

        If Debug = True Then
            WriteLog(logFolder, content) ' Sending the output to log - shall be removed when the code is in production
        End If

        ' Here we find out if we are working with results from Slalom (SL) or giant slalom (SSL)
        Dim dicipline As String = Nothing
        If content.IndexOf("Giant Slalom") <> -1 Then
            dicipline = "SSL"
        ElseIf content.IndexOf("Slalom") <> -1 Then
            dicipline = "SL"
        End If

        'Split off header
        Dim topsplit As String = "qualified for final A / B"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "FIS Points"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbLf & "Result"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbCrLf & "Result"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)


        ' This part here will be changed so that we check if the folder used is either alpint-en or alpint-to
        ' The first means that we are to parse downhill or Super-G.
        ' The second means that we will parse Super combination, combination, slalom and giant slalom. 

        ' We check if we have the job from alpint-en
        Dim res As Regex = Nothing
        Dim m As Match
        If format = FormatType.OneRound Then
            ' Run regexp to get the results for one run alpine event. 
            If Debug = True Then
                WriteLog(logFolder, "We are in one round")
            End If
            res = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ElseIf format = FormatType.TwoRounds Then
            ' Run regexp to get the results for alpine events that has two runs. 
            If Debug = True Then
                WriteLog(logFolder, "We are in two rounds")
            End If
            ' This check is just here in case NTB decides to mark the results as unofficial/official
            If content.Contains("UNOFFICIAL RESULT") Then
                WriteLog(logFolder, "We are dealing with unofficial results")
            End If

            ' The regex has been changed to use [0-9] and [A-Z] instead of \w and \d which has restrictions 
            ' res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            res = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            ' 
            ' res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            ' &nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?
            ' Linjen over fungerer p� en ssl...

        End If

        ' run the regex lines... 
        m = res.Match(content)

        ' When parsing european cup we don't get any data, so we have to run this instead:
        Dim europeancup As Boolean = False

        ' This could mean that the results are official instead
        If Not m.Success Then
            ' This is most likely because we just got a result from the european cup... 
            ' This is just a quick and dirty workaround. We should check if we find european cup in the document... 
            If format = FormatType.OneRound Then
                res = New Regex("\s+(\d+)\s+(\d+)?\s+(\d+)\s+([\w\- \/\.]+)\s+(\d+)?\s+(\w{3})\s+([\d:\,]+)\s+", _
                                RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            ElseIf format = FormatType.TwoRounds Then
                res = New Regex("\s+(\d+)\s+(\d+)?\s+(\d+)\s+([\w\- \/\.]+)\s+(\d+)?\s+(\w{3})\s+([\d:\,]+)\s+([\d:\,]+)\s+([\d:\,]+)\s+", _
                                RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            End If

            m = res.Match(content)
            europeancup = True
        End If


        While m.Success
            Dim rank As String = Nothing
            Dim bib As String = Nothing
            Dim name As String = Nothing
            Dim nation As String = Nothing
            Dim time2 As String = Nothing
            Dim time As String = Nothing
            Dim total As String = Nothing
            Dim start As String = Nothing
            Dim diff As String = Nothing
            Dim status As String = Nothing


            ' This is for debug purposes only. 
            If Debug = True Then
                Dim a As Integer

                For a = 1 To m.Groups.Count - 1
                    WriteLog(logFolder, "Dette er innholdet i m.Groups(" & a & "): " & m.Groups(a).Value.ToString)
                Next
            End If
            If format = FormatType.TwoRounds Then
                rank = m.Groups(1).Value
                bib = m.Groups(2).Value
                ' name = m.Groups(4).Value
                name = m.Groups(3).Value
                ' Removing Line Break / Carriage Return at the end of the line
                name = name.Replace(vbCr, "")
                name = name.Replace(vbCrLf, "")
                name = name.Replace(vbLf, "")
                name = ConvertTools.RotateFISName(name)
                ' nation = m.Groups(6).Value
                nation = m.Groups(4).Value
                'time = m.Groups(7).Value
                'time2 = m.Groups(8).Value
                'total = m.Groups(9).Value
                time = m.Groups(5).Value
                time2 = m.Groups(8).Value
                total = m.Groups(11).Value
            ElseIf format = FormatType.OneRound Then
                rank = m.Groups(1).Value
                bib = m.Groups(2).Value
                ' name = m.Groups(4).Value
                name = m.Groups(3).Value
                ' Removing Line Break / Carriage Return at the end of the line
                name = name.Replace(vbCr, "")
                name = name.Replace(vbCrLf, "")
                name = name.Replace(vbLf, "")
                name = ConvertTools.RotateFISName(name)
                ' nation = m.Groups(6).Value
                nation = m.Groups(4).Value
                'time = m.Groups(7).Value
                'time2 = m.Groups(8).Value
                'total = m.Groups(9).Value
                time = m.Groups(5).Value
                total = m.Groups(5).Value
            End If





            'F�rstel�pstid
            ' Dim span As TimeSpan = ConvertTools.GetTimeSpan(time, 1)
            Dim span As TimeSpan = ConvertTools.GetTimeSpan(time, 1)
            'F�rstel�pstid
            time = ConvertTools.FormatTimeAl(span)

            'Andrel�pstid
            If format = FormatType.TwoRounds Then

                If time2 <> Nothing Then
                    If Debug = True Then
                        WriteLog(logFolder, "Vi er i two rounds og vi skal konvertere annen omgang")
                    End If
                    span = ConvertTools.GetTimeSpan(time2, 1)
                    time2 = ConvertTools.FormatTimeAl(span)
                End If
            End If

            If total.ToString <> Nothing Then
                'Total tid
                span = ConvertTools.GetTimeSpan(total, 1)
                total = ConvertTools.FormatTimeAl(span)

            End If

            ' I cut from here. 



            'span = ConvertTools.GetTimeSpan(diff)
            'diff = ConvertTools.FormatTime(span)

            Dim athlete As New athlete(rank, bib, name, nation, time, time2, total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        'If none found try secondary
        If athlete_list.Count = 0 Then
            If Debug = True Then
                WriteLog(logFolder, "Trying secondary")
            End If
            Dim first As TimeSpan


            ' res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)? &nbsp;([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            res = New Regex("\s+(\d+)\s+(\d+)?\s+(\d+)\s+([\w\- \/\.]+)\s+(\d+)?\s+(\w{3})\s+([\d:\,]+)\s+([\d:\,]+)\s+([\d:\,]+)\s+", _
                            RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            m = res.Match(content)
            While m.Success

                Dim rank As String = m.Groups(1).Value
                Dim bib As String = m.Groups(2).Value
                Dim name As String = m.Groups(4).Value
                name = ConvertTools.RotateFISName(name)
                Dim nation As String = m.Groups(6).Value
                Dim start As String = ""
                Dim time As String = m.Groups(7).Value
                Dim diff As String = ""
                Dim status As String = ""
                Dim time2 As String = m.Groups(8).Value
                Dim total As String = m.Groups(9).Value

                ' Run a replace on time just in case it has a , (comma) in it
                time = Replace(time, ",", ".")
                time2 = Replace(time2.ToString, ",", ".")
                total = Replace(total.ToString, ",", ".")


                'Format the times
                If time <> "" Then
                    Dim t, d As TimeSpan
                    If rank = 1 Then
                        first = GetTimeSpan(time, 1)
                        t = first
                        d = New TimeSpan(0)
                    Else
                        t = GetTimeSpan(time, 1)
                        d = t.Subtract(first)
                    End If

                    time = ConvertTools.FormatTimeAl(t)
                    diff = ConvertTools.FormatTimeAl(d)
                End If

                Dim athlete As New athlete(rank, bib, name, nation, time, time2, total)
                athlete_list.Add(athlete)

                m = m.NextMatch()
            End While

        End If

        Return athlete_list.Count
    End Function



    'Takes a ski jumping HTML list and fills the athlete list
    Protected Function ParseDataSJ(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))

        Dim e As Encoding
        e = Encoding.GetEncoding(job.fileEncoding)
        If job.fileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If

        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^i][^>]*>", RegexOptions.Compiled Or RegexOptions.IgnoreCase)  'Keep images

        content = tags.Replace(content, "")
        content = content.Replace("&nbsp;", " ") ' Also removing the space HTML code

        'Split off header
        Dim topsplit As String = "/leer.gif"" width=""1"" height=""1"">"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Points"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)


        'Find results from primary source 1
        Dim TeamEvent As Boolean = True
        Dim res As Regex = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled)

        Dim m As Match = res.Match(content)

        'Find results from primary source 2 / Team event
        If Not m.Success Then
            TeamEvent = False

            ' We must add support for more than one REGEX
            res = New Regex("([0-9]{1,3})\s+([0-9]{1,3})\s+([0-9]{1,6})\s+(.*[^\|0-9]+)\s+[0-9]{4}\s+([A-Z]{3})\s+([0-9]{2,3}\.[0-9]{1,2})\s+(\s+|[0-9]{2,3}\.[0-9]{1,2})\s+([0-9]{2,3}\.[0-9]{1,2})\s+\n+", _
                            RegexOptions.IgnoreCase Or RegexOptions.Compiled)
            m = res.Match(content)
        End If

        While m.Success
            Dim rank As String = ""
            Dim bib As String = ""
            Dim name As String = ""
            Dim nation As String = ""
            Dim distance_1 As String = ""
            Dim distance_2 As String = ""
            Dim points_1 As String = ""
            Dim points_2 As String = ""
            Dim rank_1 As String = ""
            Dim rank_2 As String = ""
            Dim points_total As String = ""

            If TeamEvent = False Then
                rank = m.Groups(1).Value.Trim()
                bib = m.Groups(2).Value.Trim()
                name = m.Groups(4).Value.Trim()
                nation = m.Groups(5).Value.Trim()
                distance_1 = m.Groups(6).Value.Trim()
                distance_2 = m.Groups(7).Value.Trim()
                points_1 = m.Groups(8).Value.Trim()
                points_total = points_1.Trim()
            ElseIf TeamEvent = True Then
                rank = m.Groups(1).Value.Trim()
                bib = ""
                name = ""
                nation = m.Groups(3).Value.Trim()
                points_total = m.Groups(4).Value.Trim()
                distance_1 = ""
                distance_2 = ""
                points_1 = ""
                points_2 = ""
                rank_1 = ""
                rank_2 = ""



                Dim AthleteNameOne As String = m.Groups(5).Value.Trim().ToString()
                AthleteNameOne = ConvertTools.RotateFISName(AthleteNameOne)

                Dim AthleteNameTwo As String = m.Groups(8).Value.Trim().ToString()
                AthleteNameTwo = ConvertTools.RotateFISName(AthleteNameTwo)

                Dim AthleteNameThree As String = m.Groups(11).Value.Trim().ToString()
                AthleteNameThree = ConvertTools.RotateFISName(AthleteNameThree)

                Dim AthleteNameFour As String = m.Groups(14).Value.Trim().ToString()
                AthleteNameFour = ConvertTools.RotateFISName(AthleteNameFour)

                Dim AthleteNameOneJump As String = "(" & m.Groups(6).Value.ToString() & "-" & m.Groups(7).Value.ToString() & ")"
                Dim AthleteNameTwoJump As String = "(" & m.Groups(9).Value.ToString() & "-" & m.Groups(10).Value.ToString() & ")"
                Dim AthleteNameThreeJump As String = "(" & m.Groups(12).Value.ToString() & "-" & m.Groups(13).Value.ToString() & ")"
                Dim AthleteNameFourJump As String = "(" & m.Groups(15).Value.ToString() & "-" & m.Groups(16).Value.ToString() & ")"


                ' Removing -) to )
                AthleteNameOneJump = AthleteNameOneJump.Replace("-)", ")")
                AthleteNameOneJump = AthleteNameOneJump.Replace("- )", ")")
                AthleteNameOneJump = AthleteNameOneJump.Replace("-" & vbCrLf & ")", ")")
                AthleteNameOneJump = AthleteNameOneJump.Replace(" ( )", "")

                AthleteNameTwoJump = AthleteNameTwoJump.Replace("-)", ")")
                AthleteNameTwoJump = AthleteNameTwoJump.Replace("- )", ")")
                AthleteNameTwoJump = AthleteNameTwoJump.Replace("-" & vbCrLf & ")", ")")
                AthleteNameTwoJump = AthleteNameTwoJump.Replace(" ( )", "")

                AthleteNameThreeJump = AthleteNameThreeJump.Replace("-)", ")")
                AthleteNameThreeJump = AthleteNameThreeJump.Replace("- )", ")")
                AthleteNameThreeJump = AthleteNameThreeJump.Replace("-" & vbCrLf & ")", ")")
                AthleteNameThreeJump = AthleteNameThreeJump.Replace(" ( )", "")

                AthleteNameFourJump = AthleteNameFourJump.Replace("-)", ")")
                AthleteNameFourJump = AthleteNameFourJump.Replace("- )", ")")
                AthleteNameFourJump = AthleteNameFourJump.Replace("-" & vbCrLf & ")", ")")
                AthleteNameFourJump = AthleteNameFourJump.Replace(" ( )", "")


                name = " &#8211; " _
                    & AthleteNameOne & " " & AthleteNameOneJump & ", " _
                    & AthleteNameTwo & " " & AthleteNameTwoJump & ", " _
                    & AthleteNameThree & " " & AthleteNameThreeJump & ", " _
                    & AthleteNameFour & " " & AthleteNameFourJump


            End If

            Dim athlete As New athlete(rank, bib, name, nation, distance_1, distance_2, points_1, points_2, rank_1, rank_2, points_total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        'Try secondary source
        If athlete_list.Count = 0 Then

            'Find results
            res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d\.]+)\s+&nbsp;([\d\.]+)?\s+&nbsp;([\d\.]+)", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

            m = res.Match(content)
            While m.Success

                Dim rank As String = m.Groups(1).Value
                Dim bib As String = m.Groups(2).Value
                Dim name As String = m.Groups(4).Value
                Dim nation As String = m.Groups(6).Value
                Dim distance_1 As String = m.Groups(7).Value
                Dim distance_2 As String = m.Groups(8).Value
                Dim points_1 As String = ""
                Dim points_2 As String = ""
                Dim rank_1 As String = ""
                Dim rank_2 As String = ""

                Dim points_total As String = m.Groups(9).Value

                Dim athlete As New athlete(rank, bib, name, nation, distance_1, distance_2, points_1, points_2, rank_1, rank_2, points_total)
                athlete_list.Add(athlete)

                m = m.NextMatch()
            End While

        End If

        Return athlete_list.Count
    End Function
    Protected Function ParseDataCup(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer

        Dim e As Encoding
        e = Encoding.GetEncoding(job.fileEncoding)
        If job.fileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If

        Dim input As StreamReader = New StreamReader(filename, e)
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^i][^>]*>", RegexOptions.Compiled Or RegexOptions.IgnoreCase)  'Keep images
        content = tags.Replace(content, "")

        'Split off header
        Dim topsplit As String = "/leer.gif"" width=""1"" height=""1"">"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Points"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results from primary source 1
        Dim sel As Integer = 1
        Dim res As Regex = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Dim m As Match = res.Match(content)


        While m.Success
            Dim rank As String = m.Groups(3).Value
            Dim bib As String = ""
            Dim name As String = m.Groups(1).Value
            name = ConvertTools.RotateFISName(name)
            Dim nation As String = m.Groups(2).Value
            Dim distance_1 As String = ""
            Dim distance_2 As String = ""
            Dim points_1 As String = ""
            Dim points_2 As String = ""
            Dim rank_1 As String = ""
            Dim rank_2 As String = ""
            Dim points_total As String = m.Groups(4).Value

            If Debug = True Then
                Dim a As Integer
                For a = 0 To m.Groups.Count
                    WriteLog(logFolder, "Dette er verdien av m.groups(" & a & "): " & m.Groups(a).Value.ToString)
                Next
            End If

            Dim athlete As New athlete(rank, bib, name, nation, distance_1, distance_2, points_1, points_2, rank_1, rank_2, points_total)
            ' Dim athlete As New athlete(rank, "", name, nation, "", "", "", "", "", "", points_total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While



        Return athlete_list.Count
    End Function
    Public Function FIS_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String,
                                ByVal job As MainConvertModule.FolderJob) As Integer
        'Fill the athlete list
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim c As Integer = ParseDataCup(filename, job)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.points_total & ", "

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function
    'Returns a formated resultlist, for individual starts
    Public Function CC_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String,
                               ByRef job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        'Fill the athlete list
        Dim c As Integer = 0

        ' This change was made to get the sprint relay with team members
        If format = FormatType.Relay Then
            c = ParseDataCCRelay(filename, format, job)
        ElseIf format = FormatType.SprintRelay Then
            c = ParseDataCCSprintRelay(filename, format, job)
        ElseIf format = FormatType.SprintRelayTeamWithNameOnly Then
            c = ParseDataCcSprintRelayWithNamesOnly(filename, format, job)
        ElseIf format = FormatType.SprintRealyTeamWithTimeAndName Then
            c = ParseDataCCSprintRelay(filename, format, job)
        Else
            c = ParseDataCC(filename, job)
        End If

        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If




            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.IndividualStart
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                Case MainConvertModule.FormatType.Relay
                    If Not at.name.Trim().ToString() = "" Then
                        If i = 1 Then
                            ret &= at.rank & ") " & at.nation & " " & at.time & " " & at.name & ", "
                        ElseIf i = 2 Then
                            ret &= at.rank & ") " & at.nation & " " & at.diff & " min bak " & at.name & ", "
                        ElseIf i > 2 Then
                            ret &= at.rank & ") " & at.nation & " " & at.diff & " " & at.name & ", "
                        End If

                    ElseIf at.name.Trim().ToString() = "" Then
                        If i = 1 Then
                            ret &= at.rank & ") " & at.nation & " " & at.time & ", "
                        ElseIf i = 2 Then
                            ret &= at.rank & ") " & at.nation & " " & at.diff & " min bak, "
                        ElseIf i > 2 Then
                            ret &= at.rank & ") " & at.nation & " " & at.diff & ", "
                        End If

                    End If

                Case MainConvertModule.FormatType.SprintRelayTeamWithNameOnly
                    ' We are to create a list for team sprint, names only
                    If i = 1 Then
                        ret &= at.rank & ") " & at.nation & " " & at.time & " " & at.name & ", "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.nation & " " & at.diff & " min bak " & at.name & ", "

                    Else
                        ret &= at.rank & ") " & at.nation & " " & at.diff & " " & at.name & ", "
                    End If

                Case MainConvertModule.FormatType.SprintRelay
                    ' If we only have Norway II, ....
                    If Not at.name.Trim().ToString() = "" Then
                        If i = 1 Then
                            ret &= at.rank & ") " & at.nation & " " & at.time & " " & at.name & ", "
                        ElseIf i = 2 Then
                            ret &= at.rank & ") " & at.nation & " " & at.diff & " min bak " & at.name & ", "
                        ElseIf i > 2 Then
                            ret &= at.rank & ") " & at.nation & " " & at.diff & " " & at.name & ", "
                        End If
                        ' We shall render the sprint relay

                    ElseIf at.name.Trim().ToString() = "" Then
                        If i = 1 Then
                            ret &= at.rank & ") " & at.nation & " " & at.time & ", "
                        ElseIf i = 2 Then
                            ret &= at.rank & ") " & at.nation & " " & at.diff & " min bak, "
                        ElseIf i > 2 Then
                            ret &= at.rank & ") " & at.nation & " " & at.diff & ", "
                        End If

                    End If
                Case Else
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " min bak, "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & ", "
                    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        ret = ret.Replace("  ", " ")
        body = ret

        Return c
    End Function

    Public Function AL_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String,
                               ByRef job As MainConvertModule.FolderJob) As Integer
        ' -----------------------------------------------------------------------------
        ' AL_General
        '
        ' Description:
        '   Returns a formated resultlist, for alpine
        '   
        ' Parameters:
        '
        ' Returns:
        '
        ' Notes :
        '       rev 2008.01.14, Trond Hus�: Added format in call to ParseDataAL so that we get the correct format type.
        '
        ' -----------------------------------------------------------------------------

        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        'Fill the athlete list
        If Debug = True Then
            WriteLog(logFolder, "This is format: " & format.ToString)
        End If
        Dim c As Integer = ParseDataAL(filename, format, job)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.TwoRounds 'format = "TwoRounds"
                    If Debug = True Then
                        WriteLog(logFolder, "Two Rounds")
                    End If
                    at.time = Replace(at.time, "0.", "")
                    at.time2 = Replace(at.time2, "0.", "")

                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.total & " (" & at.time & "-" & at.time2 & "), "

                Case MainConvertModule.FormatType.OneRound
                    If Debug = True Then
                        WriteLog(logFolder, "One Round")
                    End If
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    'Case MainConvertModule.FormatType.KombineretNormal
                    '  ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "

                Case Else
                    ret &= " Denne falt gjennom til ELSE i fill result"



                    'Case Else
                    '   If i = 1 Then
                    '  ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    ' ' ElseIf i = 2 Then
                    ''   ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " min bak, "
                    'Else
                    '    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    'End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function
    'Returns a formated resultlist, for ski jumping
    Public Function SJ_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String,
                               ByRef job As MainConvertModule.FolderJob) As Integer

        Dim c As Integer = ParseDataSJ(filename, job)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ret &= "<p class=""txt"">"
                ' ret &= "<p class=""Brdtekst"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            Dim strSkiJumps As String = ""
            Dim TeamEvent As Boolean = False
            If at.distance_2 <> "" And at.distance_1 <> "" Then
                strSkiJumps = String.Format(" ({0}-{1}), ", at.distance_1, at.distance_2)
            ElseIf at.distance_1 <> "" And at.distance_2 = "" Then
                strSkiJumps = "(" & at.distance_1 & "), "
            ElseIf at.distance_1 = "" And at.distance_2 = "" Then
                TeamEvent = True
            End If

            If TeamEvent = False Then
                ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.points_total & strSkiJumps
            Else
                ret &= at.rank & ") " & at.nation & " " & at.points_total & " " & at.name & ", "
            End If



            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        ' Since events can be stopped after one run, we will then have a resultlist that looks bad, so we
        ' replace -) with ) to make it look correct. 
        body = Replace(body, "-)", ")")
        Return c
    End Function

    Protected Function ParseDataCcSprintRelayWithNamesOnly(ByVal filename As String, ByVal format As MainConvertModule.FormatType,
                                              ByVal job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))

        Dim e As Encoding
        e = Encoding.GetEncoding(job.FileEncoding)
        If job.FileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If


        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, "")
        content = content.Replace("&nbsp;", " ")

        WriteLog(logFolder, content)
        'Split off header
        Dim topsplit As String = "qualified for final A / B"
        If content.IndexOf(topsplit, StringComparison.Ordinal) = -1 Then
            topsplit = "FIS Points"
        End If
        If content.IndexOf(topsplit, StringComparison.Ordinal) = -1 Then
            topsplit = "Nation" & vbLf & "Result"
        End If
        If content.IndexOf(topsplit, StringComparison.Ordinal) = -1 Then
            topsplit = "Nation" & vbCrLf & "Result"
        End If
        content = content.Substring(content.IndexOf(topsplit, StringComparison.Ordinal) + topsplit.Length)

        'Try primary data scource parse

        ' The line below grabs the relay results, but only the country. 
        ' &nbsp;(\d+)\s+&nbsp;\s+([\w\- \/\.]+)&nbsp;\s+&nbsp;\s+(\w{3})\s+([\d:\.]+)\s+&nbsp;\s+&nbsp;\s+
        Dim res As Regex
        res = New Regex(job.Regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim m As Match = res.Match(content)
        If Not m.Success Then
            WriteLog(logFolder, "Cannot find any structure in the file!")
        End If


        While m.Success

            ' Added support for relay team members 29.12.2010

            Const start As String = Nothing
            Dim status As String = Nothing

            Try
                ' Some stuff is equal for both relays
                ' We are setting the rank
                Dim rank As String = m.Groups(1).Value

        
                ' We are setting the bib for the athlete, in this case to nothing
                Dim bib As String = Nothing

                ' Now getting the team members
                Dim strRelayAthleteOne As String = Replace(m.Groups(8).Value, "&nbsp;", "").ToString()
                strRelayAthleteOne = RotateFISName(strRelayAthleteOne).ToString()

                ' Second team member
                Dim strRelayAthleteTwo As String = Replace(m.Groups(9).Value, "&nbsp;", "").ToString()
                strRelayAthleteTwo = RotateFISName(strRelayAthleteTwo).ToString()

                ' Creating the list of names for the relay team
                Dim name As String = "(" & _
                    strRelayAthleteOne & ", " & _
                    strRelayAthleteTwo & ")"

                ' Time
                Dim time As String = m.Groups(4).Value.Trim().ToString()

                ' Nation
                Dim nation As String = m.Groups(3).Value.Trim().ToString()

                ' Diff
                Dim diff As String = ""

                'Format the times
                If Not time.Trim().ToString() = "Semifinale" Then
                    Dim first As TimeSpan
                    'Format the times
                    If time <> "" Then
                        Dim t, d As TimeSpan
                        If rank = 1 Then
                            first = GetTimeSpan(time, 10)
                            t = first
                            d = New TimeSpan(0)
                        Else
                            t = GetTimeSpan(time)
                            d = t.Subtract(first)
                        End If

                        time = FormatTime(t)
                        diff = FormatTime(d)
                    End If
                Else
                    time = ""
                End If


                Dim athlete As New athlete(rank, bib, name, nation, start, time, diff, status)
                athlete_list.Add(athlete)

                m = m.NextMatch()

                ' This line is here in case we get 1.02.53,8
                ' status = m.Groups(9).Value
            Catch nex As NullReferenceException
                WriteLog(logFolder, nex.ToString())
                WriteLog(logFolder, nex.Message.ToString())

                Throw New Exception(nex.Message)
            Catch nnex As NoNullAllowedException
                WriteLog(logFolder, nnex.ToString())
                WriteLog(logFolder, nnex.Message.ToString())

                Throw New Exception(nnex.Message)
            Catch ex As Exception
                WriteLog(logFolder, ex.ToString())
                WriteLog(logFolder, ex.Message.ToString())

                ' We must move the file to error folder
                Throw New Exception(ex.Message)
            End Try

        End While



        Return athlete_list.Count


    End Function

End Class
