Imports System.IO
Imports System.Text
Imports System.xml
Imports System.Configuration
Imports ntb_FuncLib.LogFile

Public Class MainConvertModule
    Inherits ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Protected Friend WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PollTimer = New System.Timers.Timer
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PollTimer
        '
        Me.PollTimer.Interval = 5000
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'Types of events
    Public Enum EventType
        CrossCountry
        SkiJump
        Biathlon
        Alpint
        Kombinert
        SpeedSkating
        Fis
        Nm
    End Enum

    'Formats
    Public Enum FormatType
        IndividualStart
        CommonStart
        Relay
        OneRound
        TwoRounds
        KombineretNormal
        KombinertStafett
        SpeedSkatingNormal
        SpeedSkatingTeamEvent
        SpeedSkatingSprint
        Fis
        SprintRelay
        SprintRelayTeamWithNameOnly
        SprintRealyTeamWithTimeAndName
    End Enum

    Public Structure FolderJob

        'Folder to poll
        Public Folder As String

        'Type og event, e.g. cross country, biathlon etc
        Public Type As EventType

        ' Which encoding the input file has
        Public FileEncoding As String

        ' Which regexp we shall use
        Public Regexpression As String

        ' Which IPTC subject code this sport has
        Public IptcRefNum As String

        ' Which IPTC Subject Code text
        Public IptcMatter As String

        'Format of resulting file
        ' ReSharper disable InconsistentNaming
        Public format As FormatType
        ' ReSharper restore InconsistentNaming

    End Structure

    Protected JobList As New ArrayList

    Public Shared NationMap As New Hashtable
    Protected FiSconvert As FIS_Live
    Protected IbUconvert As IBU_Results
    Protected ScGconvert As SCG_Results
    Protected NMconvert As FIS_NM

    Protected FileWait As Integer = 2
    Protected TimerInterval As Integer = 30000 ' 30000 til produskjonsversjonen
    Protected NotabeneError As Boolean = False

    Protected NotabeneOutput As String = ProjectFolder(ConfigurationManager.AppSettings("OutputFolder"))
    Protected DoneFolder As String = ProjectFolder(ConfigurationManager.AppSettings("DoneFolder"))
    Protected ErrorFolder As String = ProjectFolder(ConfigurationManager.AppSettings("ErrorFolder"))
    Protected LogFolder As String = ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))


    Protected Header As String
    Protected Template As String

    Public Sub Initiate()
        Dim debug As Boolean = ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        'Start
        Directory.CreateDirectory(LogFolder)
        WriteLogNoDate(LogFolder, "---------------------------------------------------------------------------------------------------------")
        WriteLog(LogFolder, "Sports results conversion starting...")

        'Load content files, (styles)
        ' We remove the old ContentTemplate.
        'Try
        'Dim reader As New StreamReader(ProjectFolder(ConfigurationManager.AppSettings("ContentTemplate")), Encoding.GetEncoding("iso-8859-1"))
        'template = reader.ReadToEnd()
        'reader.Close()
        'WriteLog(logFolder, "Content template " & ProjectFolder(ConfigurationManager.AppSettings("ContentTemplate")) & " loaded.")
        'Catch ex As Exception
        'Error
        'WriteErr(logFolder, "Error loading content template (" & ProjectFolder(ConfigurationManager.AppSettings("ContentTemplate")) & ").", ex)
        'End Try

        'Load FipHeader
        Try
            Dim reader As StreamReader = New StreamReader(ProjectFolder(ConfigurationManager.AppSettings("FipHeader")), Encoding.GetEncoding("iso-8859-1"))
            header = reader.ReadToEnd()
            reader.Close()
            WriteLog(LogFolder, "FIP header file " & ProjectFolder(ConfigurationManager.AppSettings("FipHeader")) & " loaded.")
        Catch ex As Exception
            'Error
            WriteErr(LogFolder, "Error loading FIP header (" & ProjectFolder(ConfigurationManager.AppSettings("FipHeader")) & ").", ex)
        End Try


        'Conversion objects
        FiSconvert = New FIS_Live
        FiSconvert.ErrorFolder = ErrorFolder

        IbUconvert = New IBU_Results
        IbUconvert.ErrorFolder = ErrorFolder

        ScGconvert = New SCG_Results
        ScGconvert.ErrorFolder = ErrorFolder

        NMconvert = New FIS_NM

        'Load nation map
        Dim nationmapfile As String = ProjectFolder(ConfigurationManager.AppSettings("NationMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(nationmapfile)

            Dim nodes As XmlNodeList
            Dim node As XmlNode
            nodes = map.SelectNodes("/nations/nation")

            'Fill hash
            For Each node In nodes
                NationMap(node.Attributes("code").Value) = node.Attributes("name").Value
            Next

            WriteLog(LogFolder, "Nation map loaded: " & NationMap.Count & " nations.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(LogFolder, "Error nation map (" & nationmapfile & ") file.", ex)
        End Try


        'Load folder settings
        Dim settingsfile As String = ProjectFolder(ConfigurationManager.AppSettings("FolderJobFile"))
        Try
            map.Load(settingsfile)

            Dim nodes As XmlNodeList
            Dim node As XmlNode
            nodes = map.SelectNodes("/folderJobs/job")




            'Fill hash
            For Each node In nodes
                Dim job As New FolderJob
                Try

                    job.Folder = node.Attributes("folder").Value

                    If debug Then WriteLog(LogFolder, "Job: " & node.Attributes("folder").Value)

                    job.FileEncoding = node.Attributes("encoding").Value
                    WriteLog(LogFolder, "fileEncoding: " & job.FileEncoding)
                    job.Regexpression = node.Item("regexp").InnerText
                    WriteLog(LogFolder, "Regular Expression: " & job.Regexpression)
                    job.IptcRefNum = node.Item("iptc").Attributes("refnum").Value
                    job.IptcMatter = node.Item("iptc").Attributes("matter").Value

                    Directory.CreateDirectory(job.Folder)
                Catch ex As Exception
                    WriteErr(LogFolder, "Error loading folder job list.", ex)
                End Try


                Select Case node.Attributes("type").Value
                    Case "langrenn"
                        job.Type = EventType.CrossCountry

                    Case ("skiskyting")
                        job.Type = EventType.Biathlon

                    Case "skihopp"
                        job.Type = EventType.SkiJump

                    Case "alpint"
                        job.Type = EventType.Alpint

                    Case "kombinert"
                        job.Type = EventType.Kombinert

                    Case "speedskating"
                        job.Type = EventType.SpeedSkating

                    Case "FIS"
                        job.Type = EventType.FIS

                    Case "NM"
                        job.Type = EventType.NM

                End Select

                Select Case node.Attributes("format").Value
                    Case "fellesstart"
                        job.format = FormatType.CommonStart
                    Case "enkeltstart"
                        job.format = FormatType.IndividualStart
                    Case "stafett"
                        job.format = FormatType.Relay
                    Case "enomgang"
                        job.format = FormatType.OneRound
                    Case "toomganger"
                        job.format = FormatType.TwoRounds
                    Case "kombinertnormal"
                        job.format = FormatType.KombineretNormal
                    Case "kombinertstafett"
                        job.format = FormatType.KombinertStafett
                    Case "speedskatingnormal"
                        job.format = FormatType.SpeedSkatingNormal
                    Case "speedskatingsprint"
                        job.format = FormatType.SpeedSkatingSprint
                    Case "speedskatingteamevent"
                        job.format = FormatType.SpeedSkatingTeamEvent
                    Case "FIS"
                        job.format = FormatType.Fis
                    Case "sprintstafett"
                        job.format = FormatType.SprintRelay
                    Case "sprintstafett-namesonly"
                        job.format = FormatType.SprintRelayTeamWithNameOnly

                    Case "sprintstafett-nameandtime"
                        job.format = FormatType.SprintRealyTeamWithTimeAndName

                End Select

                JobList.Add(job)
            Next

            WriteLog(LogFolder, "Job folders loaded: " & JobList.Count & " jobs.")
        Catch ex As Exception
            'Error folder job settings
            WriteErr(LogFolder, "Error loading folder job list.", ex)
        End Try

        'Other settings
        Try
            NotabeneError = ConfigurationManager.AppSettings("GenerateNotabeneError")
            TimerInterval = ConfigurationManager.AppSettings("pollTimerInterval") * 1000
            FileWait = ConfigurationManager.AppSettings("fileWait") * 1000

            Directory.CreateDirectory(NotabeneOutput)
            Directory.CreateDirectory(DoneFolder)
            Directory.CreateDirectory(ErrorFolder)
        Catch ex As Exception
            WriteErr(LogFolder, "Error loading settings.", ex)
        End Try

        'Ready to go
        PollTimer.Start()
        WriteLog(LogFolder, "Sports results conversion started.")
    End Sub

    Sub Quit()
        PollTimer.Stop()
        WriteLogNoDate(logFolder, "---------------------------------------------------------------------------------------------------------")
        WriteLog(logFolder, "Sports results conversion stopped.")
    End Sub

    Public Shared Function ProjectFolder(ByVal str As String) As String
        Return Path.Combine(ConfigurationManager.AppSettings("RootFolder"), str)
    End Function


    Private Sub PollTimer_Elapsed(ByVal sender As Object, ByVal e As Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        Dim debug As Boolean = ProjectFolder(ConfigurationManager.AppSettings("Debug"))
        'Timer kicked, loop folders
        PollTimer.Stop()

        If debug Then WriteLog(LogFolder, "In PollTimer_Elapsed")
        Dim j As FolderJob
        Dim f As String
        Dim files As String()

        Dim outfile As String
        Dim output As String
        Dim body As String = ""
        Dim count As Integer = 0

        'Loop jobs
        For Each j In JobList

            If debug Then WriteLog(LogFolder, "Job: " & j.Folder)
            'Get new files
            Try
                files = Directory.GetFiles(j.Folder)

                If debug Then WriteLog(LogFolder, "Number of files: " & files.Length.ToString())
                Threading.Thread.Sleep(FileWait)
            Catch ex As Exception
                WriteErr(LogFolder, "Error gettings files from " & j.Folder, ex)
            End Try

            'Loop files
            For Each f In files

                'Logging
                Try
                    WriteLogNoDate(LogFolder, "---------------------------------------------------------------------------------------------------------")
                    WriteLog(LogFolder, "New file: " & f)
                Catch ex As Exception
                    WriteErr(LogFolder, "Logging error.", ex)
                End Try

                'Fill header
                output = Header
                ' outfile = notabeneOutput & "\" & "WEB" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & "-" & j.type.ToString & ".htm"

                ' outfile = notabeneOutput & "\" & "WEB" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & "-" & j.type.ToString & ".xml"
                output = output.Replace("<!--id-->", "WEB" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss"))

                Dim strFilename As String = NotabeneOutput & "\" & "WEB" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & "-" & j.Type.ToString & ".xml"
                Dim intFileCounter As Integer = 0
                Do Until Not File.Exists(strFilename)
                    strFilename = NotabeneOutput & "\" & "WEB" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & "-" & intFileCounter & "-" & j.Type.ToString & ".xml"
                    intFileCounter += 1
                    WriteLog(LogFolder, "Filename: " & strFilename)

                Loop

                outfile = strFilename


                ' Doing the replacement for the coding as well
                output = output.Replace("<!--kategori-->", j.IptcMatter)
                output = output.Replace("<!--kategorikode-->", j.IptcRefNum)

                'Try to convert
                Try
                    Select Case j.Type
                        Case EventType.CrossCountry
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "langrenn-vc-res")
                            output = output.Replace("<!--tittel-->", "Langrenn:")
                            count = FiSconvert.CC_General(f, j.format, body, j)
                        Case EventType.SkiJump
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "hopp-vc-res")
                            output = output.Replace("<!--tittel-->", "Hopp:")

                            count = FiSconvert.SJ_General(f, j.format, body, j)

                        Case EventType.Biathlon
                            output = output.Replace("<!--signatur-->", "ibu")
                            output = output.Replace("<!--stikkord-->", "skiskyting-vc-res")
                            output = output.Replace("<!--tittel-->", "Skiskyting:")

                            count = IbUconvert.BI_General(f, j.format, body, j)

                        Case EventType.Alpint
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "alpint-vc-res")
                            output = output.Replace("<!--tittel-->", "Alpint:")

                            count = FiSconvert.AL_General(f, j.format, body, j)

                        Case EventType.Kombinert
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "kombinert-vc-res")
                            output = output.Replace("<!--tittel-->", "Kombinert:")

                            count = FiSconvert.KO_General(f, j.format, body, j)

                        Case EventType.SpeedSkating
                            output = output.Replace("<!--signatur-->", "scg")
                            output = output.Replace("<!--stikkord-->", "skoyter-vc-res")
                            output = output.Replace("<!--tittel-->", "Sk�yter:")
                            If debug = True Then WriteLog(LogFolder, "G�r til SCGconvert.SCG_General")
                            count = ScGconvert.SCG_General(f, j.format, body, j)

                        Case EventType.Fis
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "fis-vc-cupstilling")
                            output = output.Replace("<!--tittel-->", "FIS:")
                            If debug = True Then WriteLog(LogFolder, "G�r til FISconvert.FIS_General")
                            count = FiSconvert.FIS_General(f, j.format, body, j)

                        Case EventType.Nm
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "langrenn-nm-res")
                            output = output.Replace("<!--tittel-->", "Ski-NM:")
                            If debug = True Then WriteLog(LogFolder, "G�r til NMconvert.CC_General")
                            count = NMconvert.CC_General(f, j.format, body, j)

                    End Select

                    If count = 0 Then
                        Dim ex As New Exception("Parse error (" & j.Type.ToString & "): No items found in file '" & f & "'")
                        body = ""
                        Throw ex
                    Else
                        WriteLog(LogFolder, j.Type.ToString & " : " & count & " items found.")
                    End If

                Catch ex As Exception
                    'Conversion error
                    WriteLog(LogFolder, ex.Message)
                    WriteErr(LogFolder, "Parsing failed.", ex)
                End Try

                'Simple content check
                If body <> "" Then
                    ' body = "<p class=""Infolinje"">Infolinje</p>" & vbCrLf & "<p class=""Ingress"">Ingress</p>" & vbCrLf & body
                    body = "<p lede=""true"" class=""lead"">Ingress</p>" & vbCrLf & body
                    ' output &= template.Replace("<!--body-->", body)
                    body = body.Replace("  ", " ")
                    body = body.Replace(" , ", ", ")
                    output = output.Replace("<!--body-->", body)

                    Try
                        Dim writer As New StreamWriter(outfile, False, Encoding.GetEncoding("iso-8859-1"))
                        writer.WriteLine(output)
                        writer.Close()
                        WriteLog(LogFolder, "Data written to " & outfile)

                        strFilename = Path.GetFileNameWithoutExtension(f) & "-" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & Path.GetExtension(f)


                        File.Copy(f, DoneFolder & "\" & strFilename, True)
                        File.Delete(f)
                        WriteLog(LogFolder, f & " moved to donefolder.")
                    Catch ex As Exception
                        Try
                            'Outputfile error
                            WriteErr(LogFolder, "File output error.", ex)

                            File.Copy(f, ErrorFolder & "\" & Path.GetFileNameWithoutExtension(f) & "-" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & Path.GetExtension(f), True)
                            File.Delete(f)
                        Catch
                        End Try
                    End Try

                Else
                    'Other error, hopefully logged earlier
                    If NotabeneError Then
                        Try
                            ' body = "<p class=""Infolinje"">Konvertering feilet!</p>" & vbCrLf & "<p class=""Ingress"">Fant ingen ut�verdata for " & j.type.ToString & ".</p>" & vbCrLf
                            body = "<p lede=""true"" class=""lead"">Konvertering feilet! Fant ingen ut�verdata for " & j.Type.ToString & ".</p>" & vbCrLf

                            ' output &= template.Replace("<!--body-->", body)
                            output &= Header.Replace("<!--body-->", body)

                            Dim writer As New StreamWriter(outfile, False, Encoding.GetEncoding("iso-8859-1"))
                            writer.WriteLine(output)
                            writer.Close()
                        Catch
                        End Try
                    End If

                    Try
                        WriteLog(LogFolder, "Data missing.")
                        File.Copy(f, ErrorFolder & "\" & Path.GetFileNameWithoutExtension(f) & "-" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & Path.GetExtension(f), True)
                        File.Delete(f)
                    Catch
                    End Try
                End If
            Next

        Next

        'Restart
        PollTimer.Interval = TimerInterval
        PollTimer.Start()
    End Sub

End Class
