Imports System.IO
Imports System.xml
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports SportsConvert.MainConvertModule

Imports ntb_FuncLib.LogFile

Public Class FIS_NM

    Protected Class athlete

        'General
        Public rank As String
        Public bib As String
        Public name As String
        Public nat_code As String
        Public team As String
        Public nation As String

        'Cross country specifics
        Public start As String
        Public time As String
        Public diff As String
        Public status As String

        'Ski jumping specifics
        Public distance_1 As String
        Public distance_2 As String
        Public points_1 As String
        Public points_2 As String
        Public rank_1 As String
        Public rank_2 As String

        'Alpine specifics
        Public time2 As String
        Public total As String


        Public points_total As String

        'Constructors

        'Cross country
        ' This might have to be changed a bit if we want to do relay here as well... 
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal team_in As String, ByVal start_in As String, ByVal time_in As String, ByVal diff_in As String, ByVal status_in As String)
            rank = rank_in
            bib = bib_in
            name = name_in
            team = team_in
            nat_code = natcode_in

            nation = MainConvertModule.NationMap(nat_code)
            If nation = "" Then nation = natcode_in

            'This is cross country
            start = start_in
            time = time_in
            diff = diff_in
            status = status_in

            name = ConvertTools.RotateAndFixName(name)

            ''Character map substitutions
            'Dim subst As String() = character_map(nat_code)
            'If Not subst Is Nothing Then
            '    Dim i As Integer
            '    For i = 0 To subst.GetLength(0) - 1
            '        name = name.Replace(subst(i), subst(i + 1))
            '        i += 1
            '    Next
            'End If

        End Sub

    End Class

    Protected athlete_list As ArrayList

    Protected Shared character_map As New Hashtable
    'Object constructor creates object and loads mapping lists
    Public Sub New()

        athlete_list = New ArrayList

        'Load character map
        Dim charmapfile As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("CharacterMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(charmapfile)

            Dim nodes As XmlNodeList
            Dim nd1, nd2 As XmlNode
            nodes = map.SelectNodes("/character-maps/nation")

            'Fill hash
            For Each nd1 In nodes
                If nd1.ChildNodes().Count > 0 Then

                    Dim c As Integer = 0
                    Dim charset((nd1.ChildNodes().Count * 2) - 1) As String
                    For Each nd2 In nd1.ChildNodes()
                        charset(c) = nd2.Attributes("value").Value
                        charset(c + 1) = nd2.Attributes("replace").Value
                        c += 2
                    Next

                    character_map(nd1.Attributes("code").Value) = charset
                End If
            Next

            WriteLog(New String(ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))), "FIS - Character conversion map loaded (" & charmapfile & "): " & character_map.Count & " conversion sets.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(New String(ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))), "Error loading charachter conversion map (" & charmapfile & ") for FIS", ex)
        End Try

    End Sub

    'Takes a cross country HTML list and fills the athlete list
    Protected Function ParseDataCC_XML(ByVal filename As String) As Integer

        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim xmlDoc As XmlDocument = New XmlDocument

        xmlDoc.Load(filename)
        athlete_list.Clear()

        Dim a As XmlNode
        Dim athletes As XmlNodeList = xmlDoc.SelectNodes("/Fisresults/CC_race/CC_classified/CC_ranked")
        Dim first As TimeSpan

        For Each a In athletes

            Dim rank As String = ""
            Dim bib As String = ""
            Dim name As String = ""
            Dim nation As String = ""
            Dim start As String = ""
            Dim time As String = ""
            Dim diff As String = ""
            Dim status As String = ""

            bib = a.Item("Bib").InnerText
            rank = a.Item("Rank").InnerText
            name = a.Item("Competitor").Item("Lastname").InnerText & " " & a.Item("Competitor").Item("Firstname").InnerText
            nation = a.Item("Competitor").Item("Nation").InnerText
            time = a.Item("CC_result").Item("Totaltime").InnerText
            status = a.Attributes("Status").Value

            'Format the times
            If time <> "" Then
                Dim t, d As TimeSpan
                If rank = 1 Then
                    first = GetTimeSpan(time)
                    t = first
                    d = New TimeSpan(0)
                Else
                    t = GetTimeSpan(time)
                    d = t.Subtract(first)
                End If

                time = ConvertTools.FormatTime(t)
                diff = ConvertTools.FormatTime(d)
            End If

            Dim athlete As New athlete(rank, bib, name, nation, "", start, time, diff, status)
            athlete_list.Add(athlete)
        Next

        Return athlete_list.Count
    End Function

    'Takes a cross country HTML list and fills the athlete list
    Protected Function ParseDataCC_CSV(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer

        Dim e As Encoding
        e = Encoding.GetEncoding(job.fileEncoding)
        If job.fileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If

        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim input As TextReader = New StreamReader(filename, e)
        Dim line As String
        Dim fields As String()

        athlete_list.Clear()

        line = input.ReadLine()

        Dim rank As String = ""
        Dim bib As String = ""
        Dim name As String = ""
        Dim team As String = ""
        Dim nation As String = ""
        Dim start As String = ""
        Dim time As String = ""
        Dim diff As String = ""
        Dim status As String = ""

        Dim boolDoneTeam As Boolean = False
        Do


            fields = line.Split(";")

            If MainConvertModule.FormatType.Relay = FormatType.Relay Then
                ' I am here then.

            End If
            If fields.GetLength(0) = 44 Then

                bib = fields(2)
                rank = fields(1)
                name = fields(5)
                team = fields(6)
                nation = fields(7)
                time = fields(28)
                diff = fields(30)

                ' Split away the Pro team names
                Dim tmp As String()
                Dim splitter As Char() = "/"
                tmp = team.Split(splitter, 2)
                team = tmp(0).Trim()

                'Remove redundant shorts
                team = team.Replace(", IL", "")
                team = team.Replace(" IL", "")
                team = team.Replace(", IF", "")
                team = team.Replace(" IF", "")

                team = team.ToLower()
                team = StrConv(team, vbProperCase)

                'Format the times
                If time <> "" Then
                    Dim t, d As TimeSpan
                    t = GetTimeSpan(time)
                    d = GetTimeSpan(diff)
                    If Not t = Nothing Then
                        time = ConvertTools.FormatTime(t)
                        diff = ConvertTools.FormatTime(d)
                    End If
                End If

                Dim athlete As New athlete(rank, bib, name, nation, team, start, time, diff, status)
                athlete_list.Add(athlete)
            ElseIf fields.Length = 27 Then
                ' we are getting the team athletes
                If fields(4) = "r" Then


                    If boolDoneTeam = True Then
                        ' The we add the whole thing to the athlete object
                        Dim athlete As New athlete(rank, bib, name, nation, team, start, time, diff, status)
                        athlete_list.Add(athlete)

                        boolDoneTeam = False
                    End If

                    name = "(" & ConvertTools.RotateFISName(fields(8)) & ", "
                ElseIf fields(4) = "g" Then
                    name &= ConvertTools.RotateFISName(fields(8)) & ")"
                    boolDoneTeam = True
                ElseIf fields(4) = "y" Then
                    name = name.Replace(")", ", ")
                    name &= ConvertTools.RotateFISName(fields(8)) & ")"
                    boolDoneTeam = True
                    ' The we add the whole thing to the athlete object

                End If

            ElseIf fields.Length = 28 Then
                ' Then we are getting the team
                ' We are working on the relay!
                ' ;1;1;0;;NM11-CC-W1;IL HEMING - SKI;NOR;IL HEMING - SKI;;;;;;;;;;;39:22.4;;0.0;;;;;;Oslo
                If boolDoneTeam = True Then
                    Dim athlete As New athlete(rank, bib, name, nation, team, start, time, diff, status)
                    athlete_list.Add(athlete)
                    boolDoneTeam = False
                End If
                bib = fields(2)
                rank = fields(1)
                ' Empty names so that we don't get the whole team
                name = ""
                ' name = fields(5)

                nation = fields(7)
                time = fields(19)
                diff = fields(21)
                team = fields(6)

                'Remove redundant shorts
                team = team.Replace(" SKILAG", "")
                team = team.Replace(" 1", "")
                team = team.Replace(" SKIKLUB", "")
                team = team.Replace(" SKIKLUBB", "")
                team = team.Replace(" - SKI", "")
                team = team.Replace(" - LANGRENN", "")
                team = team.Replace(" SKI", "")
                team = team.Replace(", IL", "")
                team = team.Replace(" IL", "")
                team = team.Replace("IL ", "")
                team = team.Replace(", IF", "")
                team = team.Replace(" IF", "")
                team = team.Replace(", SK", "")
                team = team.Replace(" SK", "")
                team = team.ToLower()
                team = StrConv(team, vbProperCase)

                'Format the times
                If time <> "" Then
                    Dim t, d As TimeSpan
                    t = GetTimeSpan(time)
                    d = GetTimeSpan(diff)
                    If Not t = Nothing Then
                        time = ConvertTools.FormatTime(t)
                        diff = ConvertTools.FormatTime(d)
                    End If
                End If


            End If
            line = input.ReadLine()
        Loop Until line Is Nothing
        input.Close()

        Return athlete_list.Count
    End Function

    'Returns a formated resultlist, for individual starts
    Public Function CC_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String,
                               ByRef job As MainConvertModule.FolderJob) As Integer

        'Fill the athlete list
        Dim c As Integer = ParseDataCC_CSV(filename, job)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Or i = 2 Or i = 3 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i = 4 Then
                ret &= "<p class=""txt-ind"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.IndividualStart
                    ret &= at.rank & ") " & at.name & ", " & at.team & " " & at.time & ", "
                Case MainConvertModule.FormatType.Relay
                    If i = 1 Then
                        ' ret &= at.rank & ") " & at.team & " " & at.time & " " & at.name & ", "
                        ret &= "Gull: " & at.team & " " & at.time & " " & at.name & ",</p> " & vbCrLf
                    ElseIf i = 2 Then
                        ' ret &= at.rank & ") " & at.team & " " & at.diff & " min bak " & at.name & ", "
                        ret &= "s�lv:  " & at.team & " " & at.diff & " min bak " & at.name & ", </p>" & vbCrLf
                    ElseIf i = 3 Then
                        ' ret &= at.rank & ") " & at.team & " " & at.diff & " " & at.name & ", "
                        ret &= "bronse: " & at.team & " " & at.diff & " " & at.name & ", </p>" & vbCrLf
                    Else
                        ret &= at.rank & ") " & at.team & " " & at.diff & " " & at.name & ", "
                    End If
                Case Else
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.team & " " & at.time & ", "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.team & " " & at.diff & " min bak, "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.team & " " & at.diff & ", "
                    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function

End Class
