Imports System.IO
Imports System.xml
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports ntb_FuncLib.LogFile

Public Class IBU_Results

    Public ErrorFolder As String

    Protected Class athlete

        'General
        Public rank As String
        Public bib As String
        Public name As String
        Public nat_code As String
        Public nation As String

        'Biathlon specifics
        Public start As String
        Public time As String
        Public diff As String
        Public shootings As String
        Public shoot_total As String

        Public points_total As String

        Protected logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))

        'Constructors
        'Biathlon
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal time_in As String, ByVal diff_in As String, _
                       ByVal shootings_in As String, ByVal shoot_total_in As String)



            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in

            nation = MainConvertModule.NationMap(nat_code)
            If nation = "" Then nation = natcode_in

            time = time_in
            diff = diff_in

            shootings = shootings_in
            
            shoot_total = shoot_total_in

            name = ConvertTools.RotateAndFixName(name)

            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
            End If

        End Sub

    End Class
    Protected athlete_list As ArrayList
    Protected Shared character_map As New Hashtable

    'Object constructor creates object and loads mapping lists
    Public Sub New()

        athlete_list = New ArrayList

        'Load character map
        Dim charmapfile As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("CharacterMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(charmapfile)

            Dim nodes As XmlNodeList
            Dim nd1, nd2 As XmlNode
            nodes = map.SelectNodes("/character-maps/nation")

            'Fill hash
            For Each nd1 In nodes
                If nd1.ChildNodes().Count > 0 Then

                    Dim c As Integer = 0
                    Dim charset((nd1.ChildNodes().Count * 2) - 1) As String
                    For Each nd2 In nd1.ChildNodes()
                        charset(c) = nd2.Attributes("value").Value
                        charset(c + 1) = nd2.Attributes("replace").Value
                        c += 2
                    Next

                    character_map(nd1.Attributes("code").Value) = charset
                End If
            Next

            WriteLog(New String(MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))), "IBU - Character converison map loaded (" & charmapfile & "): " & CStr(character_map.Count) & " conversion sets.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(New String(MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))), "Error loading charachter conversion map (" & charmapfile & ") for IBU", ex)

            Throw New Exception(ex.Message)
        End Try

    End Sub

    Protected Function ParseDataBIFelles(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer
        Dim enc As Encoding

        enc = Encoding.GetEncoding(job.fileEncoding)

        If job.fileEncoding = "utf-8" Then
            enc = Encoding.UTF8
        End If
    End Function

    Protected Function ParseDataBI(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("Debug"))

        WriteLog(logFolder, "Version: 12345")

        Dim e As Encoding
        e = Encoding.GetEncoding(job.fileEncoding)
        If job.fileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If

        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        Dim tabs As Regex = New Regex("\t+", RegexOptions.Compiled) 'Drop all tags
        content = content.Replace("</td>", "|")
        content = tags.Replace(content, "")
        content = tabs.Replace(content, "")
        content = content.Replace("&nbsp;", " ")


        'Split off header
        Dim topsplit As String = "POS|BIB"
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        WriteLog(logFolder, content)

        'Find results both individual start and mass start
        Dim res As Regex = New Regex("")
        If job.format = MainConvertModule.FormatType.CommonStart Then
            res = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ElseIf job.format = MainConvertModule.FormatType.IndividualStart Then
            res = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        End If

        WriteLog(logFolder, "Running Regex: " & job.regexpression)



        'Dim first As TimeSpan
        Dim m As Match = res.Match(content)
        While m.Success
            Dim rank As String = ""
            Dim bib As String = ""
            Dim name As String = ""
            Dim nation As String = ""
            Dim shootings As String = ""
            Dim shoot_total As String = ""

            Dim time, diff As TimeSpan
            Dim strTime As String = ""
            Dim strDiff As String = ""

            Dim length As Int16 = m.Length
            Dim a As Int16
            
            If job.format = MainConvertModule.FormatType.IndividualStart Then
                rank = m.Groups(1).Value
                bib = m.Groups(2).Value
                name = m.Groups(4).Value
                nation = m.Groups(5).Value
                shootings = m.Groups(6).Value.Trim()
                shoot_total = m.Groups(7).Value


                If (m.Groups(8).Value <> "") Then
                    Dim pos As Int32 = m.Groups(8).Value.LastIndexOf(".")
                    pos = m.Groups(8).Value.Length - pos

                    If (pos > 2) Then
                        time = GetTimeSpan(m.Groups(8).Value.Replace(":", "."), 10)
                    Else
                        time = GetTimeSpan(m.Groups(8).Value.Replace(":", "."))
                    End If
                    diff = GetTimeSpan(m.Groups(9).Value.Replace(":", "."))

                    'Format times
                    If (pos > 2) Then
                        strTime = ConvertTools.FormatTime(time, 10, 2)
                    Else
                        strTime = ConvertTools.FormatTime(time)
                    End If
                    strDiff = ConvertTools.FormatTime(diff)
                End If
            ElseIf job.format = MainConvertModule.FormatType.CommonStart Then
                rank = m.Groups(1).Value
                bib = m.Groups(2).Value
                name = m.Groups(5).Value & " " & m.Groups(4).ToString().ToLowerInvariant()
                nation = m.Groups(6).Value
                shootings = m.Groups(7).Value.Trim()
                shoot_total = m.Groups(10).Value

                time = GetTimeSpan(m.Groups(11).Value.Replace(":", "."))
                diff = GetTimeSpan(m.Groups(12).Value.Replace(":", "."))

                'Format times
                Dim pos As Int32 = m.Groups(11).Value.LastIndexOf(".")
                pos = m.Groups(11).Value.Length - pos
                If (pos > 2) Then
                    strTime = ConvertTools.FormatTime(time, 10, 2)
                Else
                    strTime = ConvertTools.FormatTime(time)
                End If
                strDiff = ConvertTools.FormatTime(diff)
            End If




            Dim athlete As New athlete(rank, bib, name, nation, strTime, strDiff, shootings, shoot_total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        Return athlete_list.Count
    End Function

    Protected Function ParseDataBIRelay(ByVal filename As String, ByVal job As MainConvertModule.FolderJob) As Integer

        Dim e As Encoding
        e = Encoding.GetEncoding(job.fileEncoding.ToUpper())
        If job.fileEncoding = "utf-8" Then
            e = Encoding.UTF8
        End If

        Dim input As StreamReader = New StreamReader(filename, e)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        Dim tabs As Regex = New Regex("\t+", RegexOptions.Compiled) 'Drop all tags
        content = content.Replace("</td>", " ")
        content = tags.Replace(content, "")
        content = tabs.Replace(content, "")
        content = content.Replace("&nbsp;", " ") ' removing space (&nbsp;) HTML code
        Dim logFolder As String = New String(MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder")))
        WriteLog(logFolder, content)

        'Split off header
        Dim topsplit As String = "POS BIB"
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results
        ' ([0-9]{1,3})\|([0-9]{1,3})\|\s+\|([A-Z]+)\s+\|([A-Z]{3})\|([0-9]{1,2}\+[0-9]{1,2}\s+[0-9]{1,2}\+[0-9]{1,2})\|([0-9]{1,2}\+[0-9]{1,2})\|([0-9]{1}:[0-9]{2}:[0-9]{2}\.[0-9]{1})\|\s+([0-9]{1}\.[0-9]{1}|\+[0-9]{1,2}\.[0-9]{1}|[0-9]{1}:[0-9]{1,2}\.[0-9]{1})
        ' Dim res As Regex = New Regex("^=?(\d+|DNS|DNF)\|(\d+)\|&nbsp;\|(.+?)\|(\w{3})\|(\d*)\+?(\d*)(&nbsp;&nbsp;)?(\d*)\+?(\d*)\|\|\s*([\d\.:]+)?\|\s*\+?([\d\.:]+)?\|", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' Dim res As Regex = New Regex("([0-9]+)\|([0-9]+)\|&nbsp;\|([A-Za-z]+)\s+\|([A-Z]{3})\|([0-9\+]+)&nbsp;&nbsp;([0-9\+]+)\|([0-9\+]+)+\|([0-9\:\.]+)\|\s+([0-9\:\.\+]+)", _
        Dim res As Regex = New Regex(job.regexpression, RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        ' This line will grab some of the names on the relay team, it is marked out as it does not work perfectly.
        ' Dim res As Regex = New Regex("([0-9]+)\|([0-9]+)\|&nbsp;\|([A-Za-z]+)\s+\|([A-Z]{3})\|([0-9]+)\+([0-9]+)&nbsp;&nbsp;([0-9\+]+)\|([0-9\+])+\|([0-9\:\.]+)\|\s+([0-9\:\.\+]+)\|\s+(\|([0-9\-]+)\|&nbsp;\|(([A-z+]+)\s+([A-z+]+)|([A-z+]+)\s+([A-z+]+)\s+([A-z+]+)|([A-z+]+)\s+([A-z+]+)\s+([A-z+]+)\s+([A-z+]+))\|\|([0-9\+]+)&nbsp;&nbsp;([0-9\+]+)\|\|\|\|\s+)+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        'Dim first As TimeSpan
        Dim m As Match = res.Match(content)

        Dim i As Integer

        While m.Success

            For i = 1 To 24
                WriteLog(logFolder, "output of " & i.ToString() & ": " & m.Groups(i).Value.ToString())
            Next
            Dim rank As String = m.Groups(1).Value
            Dim bib As String = m.Groups(2).Value
            Dim name As String = "" ' m.Groups(3).Value.Trim()
            Dim nation As String = m.Groups(7).Value
            Dim shootings As String = m.Groups(8).Value.Trim()
            Dim shoot_total As String = m.Groups(9).Value.Trim()

            ' Added support for relay team members 29.12.2010
            Dim strRelayAthleteOne As String = ""
            Dim strRelayAthleteTwo As String = ""
            Dim strRelayAthleteThree As String = ""
            Dim strRelayAthleteFour As String = ""
            Dim strRelayAthleteOneTime As String = ""
            Dim strRelayAthleteTwoTime As String = ""
            Dim strRelayAthleteThreeTime As String = ""
            Dim strRelayAthleteFourTime As String = ""
            strRelayAthleteOne = m.Groups(15).Value.Trim()
            strRelayAthleteOne = ConvertTools.RotateAndFixName(strRelayAthleteOne).ToString()

            strRelayAthleteTwo = m.Groups(17).Value.Trim()
            strRelayAthleteTwo = ConvertTools.RotateAndFixName(strRelayAthleteTwo).ToString()

            strRelayAthleteThree = m.Groups(19).Value.Trim()
            strRelayAthleteThree = ConvertTools.RotateAndFixName(strRelayAthleteThree).ToString()

            strRelayAthleteFour = m.Groups(21).Value.Trim()
            strRelayAthleteFour = ConvertTools.RotateAndFixName(strRelayAthleteFour).ToString()


            ' Get penalty rounds
            strRelayAthleteOneTime = GetPenaltyAndExtraShotsBiathlon(m.Groups(16).Value.Trim())
            strRelayAthleteTwoTime = GetPenaltyAndExtraShotsBiathlon(m.Groups(18).Value.Trim())
            strRelayAthleteThreeTime = GetPenaltyAndExtraShotsBiathlon(m.Groups(20).Value.Trim())
            strRelayAthleteFourTime = GetPenaltyAndExtraShotsBiathlon(m.Groups(22).Value.Trim())

            '2011-01-03 12:10:26: output of 12: OS Alexander
            '2011-01-03 12:10:26: output of 13: 1+3  0+1
            '2011-01-03 12:10:26: output of 14: BJOERNDALEN Ole Einar
            '2011-01-03 12:10:26: output of 15: 0+0  0+0
            '2011-01-03 12:10:26: output of 16: SVENDSEN Emil Hegle
            '2011-01-03 12:10:26: output of 17: 0+0  0+1
            '2011-01-03 12:10:26: output of 18: BOE Tarjei
            '2011-01-03 12:10:26: output of 19: 0+2  0+0

            name = " &#8211; " & _
                    strRelayAthleteOne & " (" & strRelayAthleteOneTime & "), " & _
                    strRelayAthleteTwo & " (" & strRelayAthleteTwoTime & "), " & _
                    strRelayAthleteThree & " (" & strRelayAthleteThreeTime & "), " & _
                    strRelayAthleteFour & " (" & strRelayAthleteFourTime & ")"


            Dim strTime As String = ""
            Dim strDiff As String = ""

            If (m.Groups(10).Value <> "" And m.Groups(11).Value <> "") Then
                strTime = m.Groups(10).Value.Trim.Replace(".", ",").ToString()
                strTime = strTime.Replace(":", ".")

                strDiff = m.Groups(11).Value.Trim.Replace(".", ",").ToString()
                strDiff = strDiff.Replace(":", ".")

                WriteLog(logFolder, "strTime: " & strTime)
                WriteLog(logFolder, "strDiff: " & strDiff)

                ' removed below as we are getting the correct format from IBU
                'Format times
                'strTime = ConvertTools.FormatTime(time, 10, 2)
                'strDiff = ConvertTools.FormatTime(diff)
            End If

            Dim athlete As New athlete(rank, bib, name, nation, strTime, strDiff, shootings, shoot_total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        Return athlete_list.Count
    End Function

    'Returns a formated resultlist, for individual starts
    Public Function BI_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String,
                               ByRef job As MainConvertModule.FolderJob) As String
        Dim logFolder As String = New String(MainConvertModule.ProjectFolder(ConfigurationManager.AppSettings("LogFolder")))
        Dim c As Integer = ParseDataBI(filename, job)
        Dim ret As String = ""

        'Fill the athlete list
        If format = MainConvertModule.FormatType.Relay Then
            c = ParseDataBIRelay(filename, job)
        Else
            c = ParseDataBI(filename, job)
        End If

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            WriteLog(logFolder, "strDiff = " & at.diff.ToString())
            Dim strDiff As String = at.diff.ToString()
            strDiff = strDiff.Replace("+", "").ToString()

            WriteLog(logFolder, "Position of . = " & at.diff.IndexOf(".").ToString())

            If strDiff.IndexOf(".") <= 0 Then
                strDiff = "0." & strDiff
            End If

            WriteLog(logFolder, "strDiff after replace = " & strDiff.ToString())

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.Relay
                    'ret &= at.rank & ") " & at.name & ", " & at.nation & ", " & at.time & " (" & _
                    '(Convert.ToInt32(at.shoot_1) + Convert.ToInt32(at.shoot_3)) & "/" & _
                    '(Convert.ToInt32(at.shoot_2) + Convert.ToInt32(at.shoot_4)) & "), "
                    Dim strTotal As String = at.shoot_total.Replace("+", "/")

                    Dim intRank As Integer = Convert.ToInt32(at.rank)
                    If intRank = 1 Then
                        ret &= at.rank & ") " & at.nation & " " & " " & at.time & " (" & strTotal & ") " & at.name & ", "
                    ElseIf intRank = 2 Then
                        ret &= at.rank & ") " & at.nation & " " & " " & strDiff & " min bak (" & strTotal & ") " & at.name & ", "
                    ElseIf intRank > 1 Then
                        ret &= at.rank & ") " & at.nation & " " & " " & strDiff & " (" & strTotal & ") " & at.name & ", "
                    End If
                Case MainConvertModule.FormatType.IndividualStart

                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.shoot_total & "), "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & strDiff & " min bak (" & at.shoot_total & "), "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " (" & at.shoot_total & "), "
                    End If

                Case Else
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.shoot_total & "), "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " min bak (" & at.shoot_total & "), "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " (" & at.shoot_total & "), "
                    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"

        ret = ret.Replace("  ", " ")
        body = ret

        Return c
    End Function

End Class
